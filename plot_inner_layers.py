import requests
import pandas as pd
import numpy as np
import tensorflow as tf
import datetime as dt
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.use("Agg")

import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

import glob

import src.models as models
import src.grid_tricks as grid_tricks
from src.dataset import Dataset
from tensorflow.keras.models import load_model
from src.customs import (
    custom_loss,
    custom_mape,
    custom_acc,
    custom_acc_0,
    custom_acc_loss,
    false_negative,
    false_positive,
    accuracy_entropy,
    cat_false_negative,
    cat_false_positive,
    cat_accuracy_entropy,
    cat_accuracy_entropy_score,
)

from keras.models import Model

dtNow = dt.datetime.utcnow()

input_path = '/storagefinep/wrf/dataset/{date:%Y%m%d}_wrf_dataset.nc'
output_path = '/storagefinep/merge/dataset/{date:%Y%m%d}_merge_dataset.nc'
tmax_url = 'http://ftp.cptec.inpe.br/modelos/tempo/SAMeT/DAILY/TMAX/%Y/%m/SAMeT_CPTEC_TMAX_%Y%m%d.nc'
output_path_2 = {'args': ('inmemory.nc', ),
                 'kwargs': lambda _date: {'memory': requests.get(_date.strftime(tmax_url)).content}, 
                }

date_ini = '2021-01-01'
date_fin = '2021-07-27'

dates = pd.date_range(date_ini, date_fin, freq='D').to_pydatetime()
suffix = 'poc07'
# fmodel = './models/dafuq.h5'
fmodel = sorted(glob.glob(f'./models/bkp_*_aira2_{suffix}.h5'))[-1]
_scale = 1.
_ptp = 100.
# fmodel = './models/best_20210701.h5'
# fmodel = './milestone_models/res_net_aira2_poc07.h5'

print(f'using: {fmodel}')

def categorize_values(values, cats):
    _temp_values = np.zeros_like(values, dtype=int)
    _rules = _rules = [(True if left is None else values >= left) & (True if right is None else values < right) for left, right in cats]
    for idx, _rule in enumerate(_rules):
            _temp_values[_rule] = idx
    return _temp_values
    
                                          
model = models.model_perc_like((None, None, 147), loss='mse', norm=_ptp, scale=_scale)
model.load_weights(fmodel)
print(model.summary())

model = Model(inputs=model.layers[0].input, outputs=model.layers[-2].output)
model.compile(loss='mse', optimizer='adam')
'''
print('last layer weights info:',
      '\nmax:', model.layers[-1].get_weights()[0].max(),
      '\nmin:', model.layers[-1].get_weights()[0].min(),
      '\nsum:', model.layers[-1].get_weights()[0].sum(),
      '\nsquared sum:', (model.layers[-1].get_weights()[0]**2).sum(),
      '\nbias:', model.layers[-1].get_weights()[-1],
      )
'''      
_norms = {'hgtprs': 5800., 
          'tmpprs': 204., 
          'rhprs': 100., 
          'dptprs': 298., 
          'wind_speed': 30., 
          # 'wind_dir': 360., 
          'tmpsfc': 305., 
          'tmp2m': 310., 
          'rh2m': 100., 
          # 'wind_direction': 360., 
          'wind_magnitude': 30., 
          'acpcpsfc': 150., 
          'cpratsfc': 150.,
          'apcpsfc': 100.,
          'crainsfc': 100.,
          'pratesfc': 100., 
          'capesfc': 50.,
          }

use_test = True
datasets = Dataset(input_path,
                   output_path,
                   dates,
                   # stacking=True,
                   # cat=_cat,
                   norms=_norms,
                   # l_norms=[_l_norms, _l_norms_2],
                   # l_bias=[{}, _l_bias_2],
                   # test_slice_head=use_test,
                   scale=_scale,
                   ptp=_ptp,
                   )

# print(datasets._x_test.min(axis=(0, 1, 2))[:145], datasets._x_test.max(axis=(0, 1, 2))[:145], datasets._x_test.ptp(axis=(0, 1, 2))[:145])
# print(datasets._y_test.min(axis=(0, 1, 2)), datasets._y_test.max(axis=(0, 1, 2)), datasets._y_test.ptp(axis=(0, 1, 2)))
# input()

proj = ccrs.epsg(3857)

# total_power = []
def prepare_ax(datasets, *args, proj=proj):
    ax = plt.subplot(*args, projection=proj)
    ax.set_extent((datasets.general_lons.min(),
                   datasets.general_lons.max(),
                   datasets.general_lats.min(),
                   datasets.general_lats.max()),
                  crs=ccrs.PlateCarree())
    return ax
    
    
def prepare_coastline(ax):
    ax.coastlines(color='white', alpha=0.8)
    ax.gridlines(crs=ccrs.PlateCarree(), linestyle='--',
                 color='gray', draw_labels=True, alpha=0.8)


x_test, y_test = datasets.get_test(sample_size=1)
        
# test data
_in = x_test # datasets._x_test
_out = y_test # [:, 1:-1, 1:-1] # datasets._y_test_numerical

yhat = model.predict(_in[:1])[0].transpose((2, 0, 1))

# frc_maps = []
frc_full_maps = []
obs_maps = []

fig = plt.figure(figsize=(7. * 32, 7.))
for idx, layer in enumerate(yhat):
    plt.subplot(1, 32, idx + 1)
    plt.pcolormesh(layer, cmap='cividis')
    plt.colorbar()
    
plt.savefig('./imgs/inner_layers.png')
