import itertools
import numpy as np
import tensorflow as tf

from sklearn.model_selection import (
    train_test_split,
    StratifiedKFold,
)

def get_chunks(*args, target_dim=(10, 10)):
    out = []
    for arg in args:
        arg = arg.astype(np.float32)
        # arg (batch, lat, lon, feats) or (batch, timesteps, lat, lon, feats)
        ichunks = arg.shape[-3] // target_dim[0]
        jchunks = arg.shape[-2] // target_dim[1]
        islices = [slice(i * target_dim[0], (i + 1) * target_dim[0]) for i in range(ichunks)]
        jslices = [slice(j * target_dim[1], (j + 1) * target_dim[1]) for j in range(jchunks)]
        pre_alloc_arg = np.zeros((*arg.shape[:-3], ichunks * jchunks, *target_dim, arg.shape[-1]), dtype=np.float32)
        for idx, (islice, jslice) in enumerate(itertools.product(islices, jslices)):
            pre_alloc_arg[..., idx, :, :, :] = arg[..., islice, jslice, :]
        if len(pre_alloc_arg.shape) == 6:
            pre_alloc_arg = pre_alloc_arg.transpose((0, 2, 1, 3, 4, 5))
            out.append(pre_alloc_arg.reshape(-1, arg.shape[1], *target_dim, arg.shape[-1]))
        elif len(pre_alloc_arg.shape) == 5:
            out.append(pre_alloc_arg.reshape(-1, *target_dim, arg.shape[-1]))
        else:
            raise 'invalid target shape'
    return out


def get_timed_chunks(*args, target_dim=(10, 10)):
    out = []
    for arg in args:
        # arg (batch, lat, lon, feats) or (batch, timesteps, lat, lon, feats)
        ichunks = arg.shape[-3] // target_dim[0]
        jchunks = arg.shape[-2] // target_dim[1]
        islices = [slice(i * target_dim[0], (i + 1) * target_dim[0]) for i in range(ichunks)]
        jslices = [slice(j * target_dim[1], (j + 1) * target_dim[1]) for j in range(jchunks)]
        pre_alloc_arg = np.zeros((arg.shape[0], ichunks * jchunks, *target_dim, arg.shape[-1]))
        for idx, (islice, jslice) in enumerate(itertools.product(islices, jslices)):
            pre_alloc_arg[..., idx, :, :, :] = arg[..., islice, jslice, :]
        out.append(pre_alloc_arg.transpose((1, 0, 2, 3, 4)))
    return out
    
    
def get_stratified_chunks(x, y, target_dim=(10, 10), condition_max_value=[1., 0.5]):
    out = []
    
    ichunks = y.shape[-3] // target_dim[0]
    jchunks = y.shape[-2] // target_dim[1]
    islices = [slice(i * target_dim[0], (i + 1) * target_dim[0]) for i in range(ichunks)]
    jslices = [slice(j * target_dim[1], (j + 1) * target_dim[1]) for j in range(jchunks)]
    
    _x_arg = []
    _y_arg = []
    samples = 0
    for (islice, jslice) in itertools.product(islices, jslices):
        _y_chunk = y[:, islice, jslice, :]
        _condition = (_y_chunk < condition_max_value[0]).mean(axis=(1, 2, 3)) < condition_max_value[1]
        if _condition.sum() > 0:
            _x_arg.append(x[:, islice, jslice, :][_condition])
            _y_arg.append(_y_chunk[_condition])
            samples += _condition.sum()
    if samples < int(ichunks * jchunks * y.shape[0] * 0.3):
        print('less than %d samples, using condition %.2f'%(int(ichunks * jchunks * y.shape[0] * 0.3), condition_max_value[1] + 0.05))
        return get_stratified_chunks(x, y, target_dim, [condition_max_value[0], condition_max_value[1] + 0.05])
    # if samples > 10000:
    #     pick_samples = np.random.choice(np.arange(samples), size=10000, replace=False)
    #     _x_arg = np.concatenate(_x_arg, axis=0)[pick_samples]
    #     _y_arg = np.concatenate(_y_arg, axis=0)[pick_samples]
    #     return (_x_arg, _y_arg)
    return (np.concatenate(_x_arg, axis=0), np.concatenate(_y_arg, axis=0))
    

def get_stratified_shuffle_chunks(x, y, target_dim=(10, 10), target_samples=1000, categories=9):
    y_flat = np.argmax(y, axis=-1).flatten()
    x_flat = np.transpose(x, (0, 2, 3, 1, 4)).reshape(-1, x.shape[1], x.shape[4])
    # clear not enough data
    for cat in range(categories):
        mask_cat = y_flat == cat
        if mask_cat.sum() < 2:
            x_flat = x_flat[~mask_cat]
            y_flat = y_flat[~mask_cat]
        
    skf = StratifiedKFold(2)
    
    for train, valid in skf.split(x_flat, y_flat):
        x_train_singles, x_valid_singles = x_flat[train], x_flat[valid]
        y_train_singles, y_valid_singles = y_flat[train], y_flat[valid]
        break
    
    x_train, x_valid = [], []
    y_train, y_valid = [], []
    for cat in range(categories):
        mask_train = y_train_singles == cat
        mask_valid = y_valid_singles == cat
        if mask_train.sum() == 0 or mask_valid.sum() == 0:
            continue
        
        print('choosen %d samples from %d occurrencies at category %d.'%(target_samples, mask_train.sum(), cat))
        train_choosen = np.random.choice(np.arange(mask_train.sum()), size=target_samples)
        valid_choosen = np.random.choice(np.arange(mask_valid.sum()), size=target_samples)
        
        x_train.append(x_train_singles[mask_train][train_choosen])
        y_train.append(y_train_singles[mask_train][train_choosen])
        x_valid.append(x_valid_singles[mask_valid][valid_choosen])
        y_valid.append(y_valid_singles[mask_valid][valid_choosen])
        
    x_train = np.concatenate(x_train, axis=0)
    y_train = np.concatenate(y_train, axis=0)
    
    # shuffle
    shuffle_train = np.random.shuffle(np.arange(x_train.shape[0]))
    x_train = x_train[shuffle_train]
    y_train = y_train[shuffle_train]
    
    x_train = x_train.reshape(-1, *target_dim, x.shape[1], x.shape[4]).transpose((0, 3, 1, 2, 4))
    y_train = tf.one_hot(y_train.reshape(-1, *target_dim), categories).numpy()
    
    x_valid = np.concatenate(x_valid, axis=0)
    y_valid = np.concatenate(y_valid, axis=0)
    
    # shuffle
    shuffle_valid = np.random.shuffle(np.arange(x_valid.shape[0]))
    x_valid = x_valid[shuffle_valid]
    y_valid = y_valid[shuffle_valid]
    
    x_valid = x_valid.reshape(-1, *target_dim, x.shape[1], x.shape[4]).transpose((0, 3, 1, 2, 4))
    y_valid = tf.one_hot(y_valid.reshape(-1, *target_dim), categories).numpy()
    
    # print('stratify shapes check')
    # print(x_train.shape, y_train.shape, x_valid.shape, y_valid.shape)
    
    # input('press to continue')
    
    return x_train, x_valid, y_train, y_valid


class Chunks:
    def __init__(self, shape_ref, target_dim=(32, 32)):
        self.target_dim = target_dim
        self.ichunks = shape_ref[0] // target_dim[0]
        self.jchunks = shape_ref[1] // target_dim[1]
        self.chunks_per_map = int(self.ichunks * self.jchunks)
        self.remap_shape = (self.ichunks * target_dim[0],
                            self.jchunks * target_dim[1])
        self.prod_slices = None
        
        self.gen_slices()
        
        
    def gen_slices(self, ):
        self.islices = [slice(i * self.target_dim[0], (i + 1) * self.target_dim[0]) for i in range(self.ichunks)]
        self.jslices = [slice(j * self.target_dim[1], (j + 1) * self.target_dim[1]) for j in range(self.jchunks)]
        
        
    def gen_chunks(self, arg):
        pre_alloc_arg = np.zeros((arg.shape[0], self.ichunks * self.jchunks, *self.target_dim, arg.shape[-1]), dtype=np.float32)
        for idx, (islice, jslice) in enumerate(itertools.product(self.islices, self.jslices)):
            pre_alloc_arg[:, idx, :, :, :] = arg[:, islice, jslice, :]
            
        return pre_alloc_arg.reshape(-1, *self.target_dim, arg.shape[-1])
        
        
    def regen_maps(self, chunks):
        num_maps = int(chunks.shape[0] / self.chunks_per_map)
        # print('num maps:', num_maps)
        chunks = chunks.reshape(num_maps, self.chunks_per_map, *self.target_dim, chunks.shape[-1])
        maps = []
        for chunk in chunks:
            # print('chunk shape:', chunk.shape)
            _map = np.zeros((*self.remap_shape, chunks.shape[-1]))
            for c, (islice, jslice) in zip(chunk, itertools.product(self.islices, self.jslices)):
                _map[islice, jslice, :] = c[:, :, :]
            maps.append(_map.copy())
            
        return np.array(maps, dtype=np.float32)
        