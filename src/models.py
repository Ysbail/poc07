import numpy as np
import tensorflow as tf
import keras.regularizers as regularizers
import keras.backend as K
from keras.optimizers import SGD
from keras import initializers
from keras import constraints
from keras.models import (
    Sequential,
    Model
)
from keras.layers import (
    Add,
    Dot,
    ELU,
    ReLU,
    PReLU,
    Conv2D,
    Conv3D,
    Input,
    Lambda,
    Average,
    Dropout,
    Maximum,
    Permute,
    Softmax,
    Subtract,
    LeakyReLU,
    Activation,
    ConvLSTM2D,
    Concatenate,
    concatenate,
    AlphaDropout,
    MaxPooling2D,
    UpSampling2D,
    Conv2DTranspose,
    DepthwiseConv2D,
    SeparableConv2D,
    ThresholdedReLU,
    TimeDistributed,
    SpatialDropout2D,
    AveragePooling2D,
    BatchNormalization,
)
from keras.activations import (
    tanh,
    sigmoid,
    softplus,
)
from keras.constraints import (
    NonNeg,
)
from src.customs import (
    custom_loss,
    custom_mape,
    custom_acc,
    custom_acc_0,
    custom_acc_loss,
    false_positive,
    false_negative,
    below_1_acc,
    above_1_acc,
    below_10_acc,
    above_10_acc,
    gen_precision_1_acc,
    gen_recall_1_acc,
    below_above_score,
    aira_score,
    prop_false_negative,
    accuracy_entropy,
    acc_entr_loss,
    less_mistakes,
    gen_aira_f1_score,
    gen_aira_f1_1mm_score,
    gen_aira_f1_5mm_score,
    gen_aira_f1_10mm_score,
    gen_aira_f1_15mm_score,
    cat_false_positive,
    cat_false_negative,
    cat_accuracy_entropy,
    cat_accuracy_entropy_score,
)
from keras.metrics import (
    SparseCategoricalAccuracy,
    sparse_categorical_accuracy,
    categorical_accuracy,
)
from keras.losses import (
    BinaryCrossentropy,
    SparseCategoricalCrossentropy,
    CategoricalCrossentropy,
    categorical_crossentropy,
    
)
from keras.optimizers import (
    Nadam,
    SGD,
)

class ConstantValueConstraint(constraints.Constraint):
  """Constrains the elements of the tensor to `value`."""

  def __init__(self, value):
    self.value = value

  def __call__(self, w):
    return w * 0 + self.value

  def get_config(self):
    return {'value': self.value}

    
'''
def model_perc_like(shape, loss='logcosh', norm=100., scale=1.):
    _input = Input(shape=shape)
    
    out = aira2_c1_test5(_input)
    
    print('in out:', _input, out)
    
    model = Model(inputs=_input, outputs=out)
    # model.compile(loss=['sparse_categorical_crossentropy', custom_loss], optimizer='sgd', metrics=[false_positive, false_negative], loss_weights=[10., 1.]) # tf.keras.metrics.SparseTopKCategoricalAccuracy()
    model.compile(loss=loss, optimizer='adam', metrics=[below_1_acc,
                                                        above_1_acc,
                                                        below_10_acc,
                                                        above_10_acc,
                                                        gen_precision_1_acc(norm=norm, scale=scale),
                                                        gen_recall_1_acc(norm=norm, scale=scale),
                                                        gen_aira_f1_1mm_score(norm=norm, scale=scale),
                                                        gen_aira_f1_5mm_score(norm=norm, scale=scale),
                                                        gen_aira_f1_10mm_score(norm=norm, scale=scale),
                                                        gen_aira_f1_15mm_score(norm=norm, scale=scale),
                                                        gen_aira_f1_score(norm=norm, scale=scale)]) # , metrics=['acc', cat_accuracy_entropy, cat_false_positive, cat_false_negative]) 
    # SGD(learning_rate=0.001, momentum=0.9, nesterov=True)
    print(model.summary())
    
    return model
'''


def model_perc_like(shape, loss='binary_crossentropy', norm=100., scale=1., output_size=7):
    _input = Input(shape=shape)
    
    out = aira2_c1_test6(_input, output_size=output_size)
    
    print('in out:', _input, out)
    
    model = Model(inputs=_input, outputs=out)
    model.compile(loss=loss, optimizer='adam', metrics=['acc']) 
    print(model.summary())
    
    return model


def aira2_c1_test(_in, name=''):
    kernel_init = 'random_normal' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    a = inception_module(_in,  64,  96, 128, 16, 32, 32, kernel_init, bias_init)
    a = BatchNormalization()(a)
    
    b = inception_module(a, 128, 128, 192, 32, 96, 64, kernel_init, bias_init)
    b = BatchNormalization()(b)
    
    c = inception_module(b, 192,  96, 208, 16, 48, 64, kernel_init, bias_init)
    c = BatchNormalization()(c)
    
    f_3 = Conv2D(filters=1024, kernel_size=(3, 3), strides=(1, 1),
                 padding='same', activation='linear',
                 # kernel_regularizer='l1_l2'
                 )(c)
    f_3 = BatchNormalization()(f_3)
    f_3 = ReLU()(f_3)
    
    f_3 = Conv2D(filters=1,
                 kernel_size=(1, 1),
                 strides=(1, 1),
                 padding='same',
                 activation='softplus',
                 # kernel_regularizer='l1_l2',
                 bias_initializer=initializers.Constant(value=-4.6),
                 )(f_3)
                 
    # f_3 = ThresholdedReLU(theta=0.005)(f_3)
    
    return f_3


def aira2_c1_test2(_in, name=''):
    # epoch 340: 
    # loss: 2.8147e-05 - below_1_acc: 0.9906 - above_1_acc: 0.4312 - precision_1: 0.6605 - recall_1: 0.4312 - aira_f1_1mm_score: 0.5096 - aira_f1_5mm_score: 0.6496 - aira_f1_10mm_score: 0.6741 - aira_f1_15mm_score: 0.6622 - aira_f1_score: 0.1649
    # scale: 1, norm: 100
    # scaled input
    
    kernel_init = 'random_normal' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    a = inception_module(_in, 64, 96, 128, 16, 32, 32, kernel_init, bias_init)
    a = BatchNormalization()(a)
    
    b = inception_module(  a, 128, 128, 192, 32, 96, 64, kernel_init, bias_init)
    b = BatchNormalization()(b)
    
    c = inception_module(  b, 192, 96, 208, 16, 48, 64, kernel_init, bias_init)
    c = BatchNormalization()(c)
    
    f_3 = SeparableConv2D(filters=32,
                          kernel_size=(3, 3),
                          strides=(1, 1),
                          padding='same',
                          activation='relu',
                          depth_multiplier=4,
                          # kernel_regularizer='l1_l2'
                          )(c)
    
    f_3 = BatchNormalization()(f_3)
    
    f_3 = Conv2D(filters=1,
                 kernel_size=(1, 1),
                 strides=(1, 1),
                 padding='same',
                 activation='softplus',
                 # kernel_regularizer='l2',
                 bias_initializer=initializers.Constant(value=-4.6),
                 # bias_constraint=ConstantValueConstraint(-4.),
                 )(f_3)
                 
    return f_3


def aira2_c1_test3(_in, name=''):
    # epoch 165: 
    # loss: 4.4278e-05 - below_1_acc: 0.9880 - above_1_acc: 0.3463 - below_10_acc: 0.9997 - above_10_acc: 0.4257 - precision_1: 0.5511 - recall_1: 0.3463 - aira_f1_1mm_score: 0.3778 - aira_f1_5mm_score: 0.4926 - aira_f1_10mm_score: 0.5105 - aira_f1_15mm_score: 0.4743 - aira_f1_score: 0.0697 
    # scale: 1, norm: 100
    # scaled input
    
    kernel_init = 'random_normal' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    a = inception_module(_in, 64, 96, 128, 16, 32, 32, kernel_init, bias_init)
    a = BatchNormalization()(a)
    
    b = inception_module(  a, 128, 128, 192, 32, 96, 64, kernel_init, bias_init)
    b = BatchNormalization()(b)
    
    c = inception_module(  b, 192, 96, 208, 16, 48, 64, kernel_init, bias_init)
    c = BatchNormalization()(c)
    
    f_3 = Conv2D(filters=128,
                 kernel_size=(3, 3),
                 strides=(1, 1),
                 padding='same',
                 activation='relu',
                 # kernel_regularizer='l1_l2'
                 )(c)
    
    f_3 = BatchNormalization()(f_3)
    
    f_3 = Conv2D(filters=1,
                 kernel_size=(1, 1),
                 strides=(1, 1),
                 padding='same',
                 activation='softplus',
                 # kernel_regularizer='l2',
                 bias_initializer=initializers.Constant(value=-4.6),
                 # bias_constraint=ConstantValueConstraint(-4.),
                 )(f_3)
                 
    return f_3


def aira2_c1_test4(_in, name=''):
    # epoch ?
    # loss ? with same trained hours 25% above 1 acc, 80% acc when forecasting rain, unable to train fully due to lack of memory
    # scale: 10, norm: 100
    # scaled input
    kernel_init = 'random_normal' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    a = inception_module(_in, 64, 96, 128, 16, 32, 32, kernel_init, bias_init)
    a = BatchNormalization()(a)
    
    b = inception_module(  a, 128, 128, 192, 32, 96, 64, kernel_init, bias_init)
    b = BatchNormalization()(b)
    
    c = inception_module(  b, 192, 96, 208, 16, 48, 64, kernel_init, bias_init)
    c = BatchNormalization()(c)
    
    f_3 = Conv2D(filters=512,
                 kernel_size=(3, 3),
                 strides=(1, 1),
                 padding='same',
                 activation='relu',
                 # kernel_regularizer='l1_l2'
                 )(c)
    
    f_3 = BatchNormalization()(f_3)
    
    f_3 = Conv2D(filters=1,
                 kernel_size=(1, 1),
                 strides=(1, 1),
                 padding='same',
                 activation='softplus',
                 # kernel_regularizer='l2',
                 bias_initializer=initializers.Constant(value=-4.6),
                 # bias_constraint=ConstantValueConstraint(-4.),
                 )(f_3)
                 
    return f_3
                

def aira2_c1_test5(_in, name=''):
    # epoch 912
    #loss: 1.1123e-05 - below_1_acc: 0.9943 - above_1_acc: 0.5925 - below_10_acc: 0.9998 - above_10_acc: 0.8176 - precision_1: 0.8175 - recall_1: 0.5925 - aira_f1_1mm_score: 0.6856 - aira_f1_5mm_score: 0.8217 - aira_f1_10mm_score: 0.8454 - aira_f1_15mm_score: 0.8387 - aira_f1_score: 0.4050
    # scale: 1, norm: 100
    # scaled input
    kernel_init = 'random_normal' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    a = inception_module(_in, 192, 128, 208, 32, 96, 64, kernel_init, bias_init)
    a = BatchNormalization()(a)
    
    b = inception_module(  a, 192, 128, 208, 32, 96, 64, kernel_init, bias_init)
    b = BatchNormalization()(b)
    
    c = inception_module(  b, 192, 128, 208, 32, 96, 64, kernel_init, bias_init)
    c = BatchNormalization()(c)
    
    f_3 = Conv2D(filters=256,
                 kernel_size=(1, 1),
                 strides=(1, 1),
                 padding='same',
                 activation='relu',
                 # kernel_regularizer='l1_l2'
                 )(c)
    
    f_3 = BatchNormalization()(f_3)
    
    f_3 = Conv2D(filters=1,
                 kernel_size=(1, 1),
                 strides=(1, 1),
                 padding='same',
                 activation='softplus',
                 # kernel_regularizer='l2',
                 bias_initializer=initializers.Constant(value=-4.6),
                 # bias_constraint=ConstantValueConstraint(-4.),
                 )(f_3)
                 
    return f_3
    

def aira2_c1_test6(_in, _orig_shape=147, name='', depth=4, output_size=7):
    kernel_init = 'random_normal' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    a = BatchNormalization()(_in)

    for _ in range(depth):
        a = inception_module_aira(a, _orig_shape, kernel_init, bias_init)
    
    
    f_3 = Conv2D(filters=1024,
                 kernel_size=(3, 3),
                 strides=(1, 1),
                 padding='same',
                 activation='relu',
                 kernel_regularizer='l2'
                 )(a)

    out = []
    for _ in range(output_size):
        out.append(Conv2D(filters=2,
                          kernel_size=(1, 1),
                          strides=(1, 1),
                          padding='same',
                          activation='softmax',
                          kernel_regularizer='l2',
                          # bias_initializer=initializers.Constant(value=-4.6),
                          )(f_3))
                 
    return out
    
            
def gambi_res_net_n_blocks(_in, _filters, num_blocks):

    # c_0 = BatchNormalization()(_in)
    c_0_block = _in
    c_0_output = _in
    
    for _ in range(num_blocks):
        c_0_drop, c_0_full = v1_res_block(c_0_block, _filters)
        
        c_0_block = Add()([c_0_output, c_0_full])
        c_0_output = Add()([c_0_output, c_0_drop])
        
    c_n = c_0_output
    
    out = Conv2D(filters=1,
                 kernel_size=(3, 3),
                 strides=(1, 1),
                 padding='valid',
                 activation='softplus',
                 # kernel_regularizer='l1_l2',
                 # bias_initializer=initializers.Constant(value=-4.6),
                 # bias_constraint=ConstantValueConstraint(-4.6),
                 )(c_n)
    
    # out = Lambda(lambda x: K.clip(x, 0., 1.))(out)
    
    
    return out
    

def v1_res_block(_in, _filters=128):

    c0 = DepthwiseConv2D(kernel_size=(1, 1),
                         strides=(1, 1),
                         padding='same',
                         activation='relu',
                         # kernel_regularizer='l1_l2',
                         )(_in)
                
    # c0 = BatchNormalization()(c0)
    # c0 = ReLU()(c0)
    
    c1 = SeparableConv2D(filters=_filters,
                         kernel_size=(3, 3),
                         strides=(1, 1),
                         padding='same',
                         activation='relu',
                         depth_multiplier=4,
                         # depthwise_regularizer='l1_l2',
                         # pointwise_regularizer='l1_l2',
                         )(c0)
    
    # c1 = BatchNormalization()(c1)
    # c1 = ReLU()(c1)
    
    c2 = Conv2D(filters=_filters,
                kernel_size=(1, 1),
                strides=(1, 1),
                padding='same',
                activation='linear',
                # kernel_regularizer='l1_l2',
                )(c1)
    
    out_drop = SpatialDropout2D(0.5)(c2)
    out = c2
    
    return out_drop, out
        
    
def v1_res_block_depth(_in):

    c0 = DepthwiseConv2D(kernel_size=(1, 1),
                          strides=(1, 1),
                          padding='same',
                          activation='linear',
                          depth_multiplier=1,
                          depthwise_regularizer='l2'
                          )(_in)
                          
    c0 = BatchNormalization()(c0)
    c0 = ReLU()(c0)
    
    c1 = DepthwiseConv2D(kernel_size=(3, 3),
                          strides=(1, 1),
                          padding='same',
                          activation='linear',
                          depth_multiplier=1,
                          depthwise_regularizer='l2'
                          )(c0)
                          
    c1 = BatchNormalization()(c1)
    c1 = ReLU()(c1)
    
    c2 = DepthwiseConv2D(kernel_size=(1, 1),
                          strides=(1, 1),
                          padding='same',
                          activation='softsign',
                          depth_multiplier=1,
                          depthwise_regularizer='l2'
                          )(c1)
    
    out = Add()([_in, c2])
    
    return out
    
       
def model_multi(shape, multi=6, loss='categorical_crossentropy'):
    _input = Input(shape=shape)
    
    out = aira2_c4_multi(_input, multi=multi)
    
    model = Model(inputs=_input, outputs=out)
    model.compile(loss=loss, optimizer='nadam', metrics=[categorical_accuracy, cat_accuracy_entropy, cat_false_positive, cat_false_negative, cat_accuracy_entropy_score, less_mistakes])
    
    return model

def model_3d(shape, loss='categorical_crossentropy'):
    _input = Input(shape=shape)
    
    out = simple_3d_model(_input, name='3d')
    print(_input, out)
    
    model = Model(inputs=_input, outputs=out)
    model.compile(loss=loss, optimizer='adam', metrics=[categorical_accuracy, cat_accuracy_entropy, cat_false_positive, cat_false_negative, cat_accuracy_entropy_score, less_mistakes])
    print(model.summary())
    
    return model
    

def model_stack(shape, n_weak=10):
    _input = Input(shape=shape)
    
    weak = []
    for i in range(n_weak):
        weak.append(mini_model(_input, name=str(i)))
    _meta_input = concatenate(weak)
    _meta = mini_model(_meta_input, name='meta')
    
    model = Model(inputs=_input, outputs=weak + [_meta])
    model.compile(loss='msle', optimizer='adam', metrics=['mse', 'mae', custom_mape, custom_acc, custom_acc_0, custom_acc_loss])
    
    print(model.summary())
    
    return model


def model_var_sequential(shape, ):
    _input = Input(shape=shape)
    out = []
    for i in range(shape[-1]):
        if len(out) > 1:
            out.append( mini_model( concatenate( [out[-1], Lambda(lambda x: x[:, :, :, i:i+1])(_input)] ) ) )
        else:
            out.append( mini_model( Lambda(lambda x: x[:, :, :, i:i+1])(_input) ) ) 
        
    model = Model(inputs=_input, outputs=out)
    model.compile(loss='logcosh', optimizer='sgd', metrics=[custom_acc, custom_acc_0, custom_acc_loss])
    
    print(model.summary())
    
    return model

def aira2_c4_multi(_in, multi=6):
    # .dot([0, 15, 30, 100])
    kernel_init = 'random_normal' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    _in = concatenate([_in, BatchNormalization(axis=-1)(_in)])
    
    a = inception_module(_in,  64,  96, 128, 16, 32, 32, kernel_init, bias_init)
    a = BatchNormalization(axis=-1)(a)
    # a = SpatialDropout2D(0.2)(a)
    b = inception_module(a, 128, 128, 192, 32, 96, 64, kernel_init, bias_init)
    b = BatchNormalization(axis=-1)(b)
    # b = SpatialDropout2D(0.2)(b)
    c = inception_module(b, 192,  96, 208, 16, 48, 64, kernel_init, bias_init)
    c = BatchNormalization(axis=-1)(c)
    c = SpatialDropout2D(0.4)(c)
    f_3 = Conv2D(filters=1024, kernel_size=(3, 3), strides=(1, 1),
                 padding='same', activation='relu', kernel_regularizer='l2')(c)
    f_3 = BatchNormalization(axis=-1)(f_3)
    out = []
    for i in range(multi):
        out.append(Conv2D(filters=4,
                          kernel_size=(1, 1),
                          strides=(1, 1),
                          padding='same',
                          activation='softmax',
                          kernel_regularizer='l2',
                          name='cat_%d'%i)(f_3))
    return out
        
def mini_model(_in, name=''):
    a   = Conv2D(filters=64, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='relu')(_in)
    b   = Conv2D(filters=32, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='relu')(a)
    c   = Conv2D(filters=16, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='relu')(b)
    d_1 = Conv2D(filters= 1, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='tanh')(c)
    d_2 = ReLU(name=name)(d_1)
    return d_2 


def weak_model(_in, name=''):
    
    a_1 = Conv2D(filters=16, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='tanh')(_in)
    # a_3 = Conv2D(filters=16, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='tanh')(_in)
    # a_5 = Conv2D(filters=16, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='tanh')(_in)
    # a_c = concatenate([a_1, a_3, a_5])

    
    b_1 = Conv2D(filters=8, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='sigmoid')(a_1)
    # b_3 = Conv2D(filters=8, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='sigmoid')(a_c)
    # b_5 = Conv2D(filters=8, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='sigmoid')(a_c)
    # b_c = concatenate([b_1, b_3, b_5])
    
    c_1 = Conv2D(filters=4, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='tanh')(b_1)
    # c_3 = Conv2D(filters=4, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='tanh')(b_c)
    # c_5 = Conv2D(filters=4, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='tanh')(b_c)
    # c_c = concatenate([c_1, c_3, c_5])

    f_1 = Conv2D(filters=  1, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='sigmoid')(c_1)
    
    return f_1
    
def medium_model(_in, name='numerical'):
    
    # _in = BatchNormalization()(_in)
    
    kernel_init = initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    a_neurons = (np.array([64, 96, 128, 16, 32, 32])).astype(int) # 64 + 128 + 32 + 32 = 256
    b_neurons = (np.array([128, 128, 192, 32, 96, 64])).astype(int) # 128 + 192 + 96 + 64 = 480
    c_neurons = (np.array([192, 96, 208, 16, 48, 64])).astype(int) # 192 + 208 + 48 + 64 = 512
    
    # a_neurons = (np.array([256, 160, 320, 32, 128, 128])).astype(int) # 64 + 128 + 32 + 32 = 256
    # b_neurons = (np.array([256, 160, 320, 32, 128, 128])).astype(int) # 128 + 192 + 96 + 64 = 480
    # c_neurons = (np.array([384, 192, 384, 48, 128, 128])).astype(int) # 192 + 208 + 48 + 64 = 512

    # a = inception_module(_in, *a_neurons, kernel_init, bias_init)
    a = inception_module(_in, *c_neurons, kernel_init, bias_init)
    a = BatchNormalization()(a)
    # b = inception_module(a, *b_neurons, kernel_init, bias_init)
    b = inception_module(a, *b_neurons, kernel_init, bias_init)
    b = BatchNormalization()(b)
    # c = inception_module(b, *c_neurons, kernel_init, bias_init)
    c = inception_module(b, *a_neurons, kernel_init, bias_init)
    c = BatchNormalization()(c)
    # c = Dropout(0.5)(c)
    # c = SpatialDropout2D(0.5)(c)
    
    # c = MaxPooling2D(pool_size=(3, 3), strides=(1, 1), padding='same')(c)
    
    f_3 = Conv2D(filters=1024, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='relu', bias_initializer=bias_init)(c)
    # f_3 = BatchNormalization()(f_3)
    # f_3 = Conv2D(filters=512, kernel_size=(1, 1), strides=(1, 1),
    #              padding='same', activation='relu')(f_3)
    # f_3 = BatchNormalization()(f_3)
    # f_3 = Conv2D(filters=256, kernel_size=(1, 1), strides=(1, 1),
    #              padding='same', activation='relu')(f_3)
    # f_3 = BatchNormalization()(f_3)
    # f_3 = Conv2D(filters=64, kernel_size=(1, 1), strides=(1, 1),
    #              padding='same', activation='relu', kernel_regularizer='l1', bias_regularizer='l1')(f_3)
    # f_3 = Conv2D(filters=16, kernel_size=(1, 1), strides=(1, 1),
    #              padding='same', activation='relu', kernel_regularizer='l1', bias_regularizer='l1')(f_3)
    f_4 = Conv2D(filters=1, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='relu', name=name, bias_initializer=bias_init)(f_3)
    # f_4 = ReLU()(f_4)

    return f_4
    
    
def gambi_model(_in, shape):
    c_p = ConvLSTM2D(filters=128, kernel_size=(1, 1), strides=(1, 1), padding='same', return_sequences=True, activation='tanh')(_in)
    c_q = ConvLSTM2D(filters=64, kernel_size=(1, 1), strides=(1, 1), padding='same', return_sequences=True, activation='tanh')(c_p)
    c_r = ConvLSTM2D(filters=32, kernel_size=(1, 1), strides=(1, 1), padding='same', return_sequences=True, activation='tanh')(c_q)
    c_q1 = ConvLSTM2D(filters=64, kernel_size=(1, 1), strides=(1, 1), padding='same', return_sequences=True, activation='tanh')(c_r)
    c_p1 = ConvLSTM2D(filters=128, kernel_size=(1, 1), strides=(1, 1), padding='same', return_sequences=True, activation='tanh')(Add()([c_q1, c_q]))
    c_in = ConvLSTM2D(filters=shape[-1], kernel_size=(1, 1), strides=(1, 1), padding='same', return_sequences=True, activation='sigmoid')(Add()([c_p1, c_p]))
    ######################
    # c_r = BatchNormalization()(c_r)
    c_r_drop = TimeDistributed(SpatialDropout2D(0.5))(c_r)
    c_0 = ConvLSTM2D(filters=32, kernel_size=(1, 1), strides=(1, 1), padding='same', return_sequences=True, activation='tanh')(c_r_drop)
    # c_0 = BatchNormalization()(c_0)
    c_0_drop = TimeDistributed(SpatialDropout2D(0.5))(c_0)
    c_1 = ConvLSTM2D(filters=32, kernel_size=(1, 1), strides=(1, 1), padding='same', return_sequences=True, activation='tanh')(Add()([c_r, c_0_drop]))
    # c_1 = BatchNormalization()(c_1)
    c_1_drop = TimeDistributed(SpatialDropout2D(0.5))(c_1)
    c_2 = ConvLSTM2D(filters=32, kernel_size=(1, 1), strides=(1, 1), padding='same', return_sequences=True, activation='tanh')(Add()([c_r, c_0, c_1_drop]))
    # c_2 = BatchNormalization()(c_2)
    c_2_drop = TimeDistributed(SpatialDropout2D(0.5))(c_2)
    c_3 = ConvLSTM2D(filters=1, kernel_size=(1, 1), strides=(1, 1), padding='same', return_sequences=True, activation='sigmoid', name='output')(Add()([c_r, c_0, c_1, c_2_drop]))

    return c_in, c_3
    
    
def gambi_res_net_8_blocks(_in, _filters=128):
    
    v_0 = Lambda(lambda x: x[..., :22])(_in)
    v_1 = Lambda(lambda x: x[..., 22:44])(_in)
    v_2 = Lambda(lambda x: x[..., 44:66])(_in)
    v_3 = Lambda(lambda x: x[..., 66:88])(_in)
    v_4 = Lambda(lambda x: x[..., 88:110])(_in)
    v_5 = Lambda(lambda x: x[..., 110:132])(_in)
    v_6 = Lambda(lambda x: x[..., 132:])(_in)
    
    c_v_0 = SeparableConv2D(filters=16,
                            kernel_size=(3, 3),
                            strides=(1, 1),
                            padding='same',
                            activation='softplus',
                            depth_multiplier=4,
                            # pointwise_regularizer='l1_l2',
                            # depthwise_regularizer='l1_l2',
                            )(v_0)
    c_v_1 = SeparableConv2D(filters=16,
                            kernel_size=(3, 3),
                            strides=(1, 1),
                            padding='same',
                            activation='softplus',
                            depth_multiplier=4,
                            # pointwise_regularizer='l1_l2',
                            # depthwise_regularizer='l1_l2',
                            )(v_1)
    c_v_2 = SeparableConv2D(filters=16,
                            kernel_size=(3, 3),
                            strides=(1, 1),
                            padding='same',
                            activation='softplus',
                            depth_multiplier=4,
                            # pointwise_regularizer='l1_l2',
                            # depthwise_regularizer='l1_l2',
                            )(v_2)
    c_v_3 = SeparableConv2D(filters=16,
                            kernel_size=(3, 3),
                            strides=(1, 1),
                            padding='same',
                            activation='softplus',
                            depth_multiplier=4,
                            # pointwise_regularizer='l1_l2',
                            # depthwise_regularizer='l1_l2',
                            )(v_3)
    c_v_4 = SeparableConv2D(filters=16,
                            kernel_size=(3, 3),
                            strides=(1, 1),
                            padding='same',
                            activation='softplus',
                            depth_multiplier=4,
                            # pointwise_regularizer='l1_l2',
                            # depthwise_regularizer='l1_l2',
                            )(v_4)
    c_v_5 = SeparableConv2D(filters=16,
                            kernel_size=(3, 3),
                            strides=(1, 1),
                            padding='same',
                            activation='softplus',
                            depth_multiplier=4,
                            # pointwise_regularizer='l1_l2',
                            # depthwise_regularizer='l1_l2',
                            )(v_5)
    c_v_6 = SeparableConv2D(filters=32,
                            kernel_size=(3, 3),
                            strides=(1, 1),
                            padding='same',
                            activation='softplus',
                            depth_multiplier=4,
                            # pointwise_regularizer='l1_l2',
                            # depthwise_regularizer='l1_l2',
                            )(v_6)
    
    c_0 = concatenate([c_v_0, c_v_1, c_v_2, c_v_3, c_v_4, c_v_5, c_v_6])
    c_0 = BatchNormalization()(c_0)
    
    c_1 = v1_res_block(c_0)
    c_2 = v1_res_block(c_1)
    c_3 = v1_res_block(c_2)
    c_4 = v1_res_block(c_3)
    c_5 = v1_res_block(c_4)
    c_6 = v1_res_block(c_5)
    c_7 = v1_res_block(c_6)
    lst = v1_res_block(c_7)

    out = SeparableConv2D(filters=1,
                          kernel_size=(3, 3),
                          strides=(1, 1),
                          padding='same',
                          activation='softplus',
                          depth_multiplier=4,
                          # pointwise_regularizer='l2',
                          # depthwise_regularizer='l2',
                          )(lst)
    
    return out





def v1_res_block_simple(_in, _filters=128):
    c_0 = Conv2D(filters=_filters, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu',
                           kernel_regularizer='l1')(_in)
    c_1 = Conv2D(filters=_filters, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='linear',
                           kernel_regularizer='l1')(c_0)
    
    c_1 = SpatialDropout2D(0.5)(c_1)
    c_1 = Add()([_in, c_1])
    c_1 = ReLU()(c_1)
    c_1 = BatchNormalization()(c_1)
    
    return c_1
    


    
    
def gambi_res_net_v2(_in):
    
    _in = BatchNormalization()(_in)
    _in = Permute((3, 1, 2))(_in)
    
    v_0 = Lambda(lambda x: x[:, :22])(_in)
    v_1 = Lambda(lambda x: x[:, 22:44])(_in)
    v_2 = Lambda(lambda x: x[:, 44:66])(_in)
    v_3 = Lambda(lambda x: x[:, 66:88])(_in)
    v_4 = Lambda(lambda x: x[:, 88:110])(_in)
    v_5 = Lambda(lambda x: x[:, 110:132])(_in)
    v_6 = Lambda(lambda x: x[:, 132:])(_in)
    
    c_v_0 = SeparableConv2D(filters=8, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu', depth_multiplier=4, data_format='channels_first')(v_0)
    c_v_1 = SeparableConv2D(filters=8, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu', depth_multiplier=4, data_format='channels_first')(v_1)
    c_v_2 = SeparableConv2D(filters=8, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu', depth_multiplier=4, data_format='channels_first')(v_2)
    c_v_3 = SeparableConv2D(filters=8, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu', depth_multiplier=4, data_format='channels_first')(v_3)
    c_v_4 = SeparableConv2D(filters=8, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu', depth_multiplier=4, data_format='channels_first')(v_4)
    c_v_5 = SeparableConv2D(filters=8, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu', depth_multiplier=4, data_format='channels_first')(v_5)
    c_v_6 = SeparableConv2D(filters=16, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu', depth_multiplier=4, data_format='channels_first')(v_6)
    
    c_0 = Concatenate(axis=1)([c_v_0, c_v_1, c_v_2, c_v_3, c_v_4, c_v_5, c_v_6])
    c_0 = BatchNormalization(axis=1)(c_0)
    
    c_1 = SeparableConv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu', depth_multiplier=4, data_format='channels_first')(c_0)
    c_2 = SeparableConv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='linear', depth_multiplier=4, data_format='channels_first')(c_1)
    
    c_2 = SpatialDropout2D(0.5, data_format='channels_first')(c_2)
    c_2 = Add()([c_0, c_2])
    c_2 = Activation('softplus')(c_2)
    c_2 = BatchNormalization(axis=1)(c_2)
    
    c_3 = SeparableConv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu', depth_multiplier=4, data_format='channels_first')(c_2)
    c_4 = SeparableConv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='linear', depth_multiplier=4, data_format='channels_first')(c_3)
    
    c_4 = SpatialDropout2D(0.5, data_format='channels_first')(c_4)
    c_4 = Add()([c_2, c_4])
    c_4 = Activation('softplus')(c_4)
    c_4 = BatchNormalization(axis=1)(c_4)
    
    c_5 = SeparableConv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu', depth_multiplier=4, data_format='channels_first')(c_4)
    c_6 = SeparableConv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='linear', depth_multiplier=4, data_format='channels_first')(c_5)
    
    c_6 = SpatialDropout2D(0.5, data_format='channels_first')(c_6)
    c_6 = Add()([c_4, c_6])
    c_6 = Activation('softplus')(c_6)
    c_6 = BatchNormalization(axis=1)(c_6)
    
    c_7 = SeparableConv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu', depth_multiplier=4, data_format='channels_first')(c_6)
    c_8 = SeparableConv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='linear', depth_multiplier=4, data_format='channels_first')(c_7)
    
    c_8 = SpatialDropout2D(0.5, data_format='channels_first')(c_8)
    c_8 = Add()([c_6, c_8])
    c_8 = Activation('softplus')(c_8)
    c_8 = BatchNormalization(axis=1)(c_8)
    
    c_9 =  SeparableConv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu', depth_multiplier=4, data_format='channels_first')(c_8)
    c_10 = SeparableConv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='linear', depth_multiplier=4, data_format='channels_first')(c_9)
    
    c_10 = SpatialDropout2D(0.5, data_format='channels_first')(c_10)
    c_10 = Add()([c_8, c_10])
    c_10 = Activation('softplus')(c_10)
    c_10 = BatchNormalization(axis=1)(c_10)

    c_11 = SeparableConv2D(filters=1, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='softplus', depth_multiplier=16, data_format='channels_first')(c_10)
    
    c_11 = Permute((2, 3, 1))(c_11)
    
    return c_11


def gambi_res_net_v3(_in):
    
    _in = Permute((3, 1, 2))(_in) # NHWC to NCHW
    
    v_0 = Lambda(lambda x: x[:, :22])(_in)
    v_1 = Lambda(lambda x: x[:, 22:44])(_in)
    v_2 = Lambda(lambda x: x[:, 44:66])(_in)
    v_3 = Lambda(lambda x: x[:, 66:88])(_in)
    v_4 = Lambda(lambda x: x[:, 88:110])(_in)
    v_5 = Lambda(lambda x: x[:, 110:132])(_in)
    v_6 = Lambda(lambda x: x[:, 132:])(_in)
    
    c_v_0 = SeparableConv2D(filters= 8, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='selu', depth_multiplier=4, data_format='channels_first', pointwise_initializer='lecun_normal', depthwise_initializer='lecun_normal')(v_0)
    c_v_1 = SeparableConv2D(filters= 8, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='selu', depth_multiplier=4, data_format='channels_first', pointwise_initializer='lecun_normal', depthwise_initializer='lecun_normal')(v_1)
    c_v_2 = SeparableConv2D(filters= 8, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='selu', depth_multiplier=4, data_format='channels_first', pointwise_initializer='lecun_normal', depthwise_initializer='lecun_normal')(v_2)
    c_v_3 = SeparableConv2D(filters= 8, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='selu', depth_multiplier=4, data_format='channels_first', pointwise_initializer='lecun_normal', depthwise_initializer='lecun_normal')(v_3)
    c_v_4 = SeparableConv2D(filters= 8, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='selu', depth_multiplier=4, data_format='channels_first', pointwise_initializer='lecun_normal', depthwise_initializer='lecun_normal')(v_4)
    c_v_5 = SeparableConv2D(filters= 8, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='selu', depth_multiplier=4, data_format='channels_first', pointwise_initializer='lecun_normal', depthwise_initializer='lecun_normal')(v_5)
    c_v_6 = SeparableConv2D(filters=16, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='selu', depth_multiplier=4, data_format='channels_first', pointwise_initializer='lecun_normal', depthwise_initializer='lecun_normal')(v_6)
    
    c_0 = Concatenate(axis=1)([c_v_0, c_v_1, c_v_2, c_v_3, c_v_4, c_v_5, c_v_6])
    
    c_1 = v3_res_block(c_0)
    c_2 = v3_res_block(c_1)
    c_3 = v3_res_block(c_2)
    c_4 = v3_res_block(c_3)
    c_5 = v3_res_block(c_4)

    c_6 = SeparableConv2D(filters=1, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='softplus', depth_multiplier=16, data_format='channels_first')(c_5)
    
    c_6 = Permute((2, 3, 1))(c_6) # NCHW to NHWC
    
    return c_6


def v3_res_block(_in):
    c_0 =  SeparableConv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='selu', depth_multiplier=4, data_format='channels_first', pointwise_initializer='lecun_normal', depthwise_initializer='lecun_normal')(_in)
    c_1 = SeparableConv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='selu', depth_multiplier=4, data_format='channels_first', pointwise_initializer='lecun_normal', depthwise_initializer='lecun_normal')(c_0)
    c_1 = AlphaDropout(0.5)(c_1)
    c_1 = Add()([_in, c_1])
    # c_1 = Activation('selu')(c_1)
    return c_1
        
def timed_gambi_res_net(_in):
    
    _in = BatchNormalization()(_in)
    
    c_0 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu'))(_in)
    
    c_1 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu'))(c_0)
    c_2 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='linear'))(c_1)
    
    c_2 = Add()([c_0, c_2])
    c_2 = ReLU()(c_2)
    c_2 = BatchNormalization()(c_2)
    
    c_3 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu'))(c_2)
    c_4 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='linear'))(c_3)
    
    c_4 = Add()([c_2, c_4])
    c_4 = ReLU()(c_4)
    c_4 = BatchNormalization()(c_4)
    
    c_7 = TimeDistributed(Conv2D(filters=256, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu'))(c_4)
    c_8 = TimeDistributed(Conv2D(filters=256, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='linear'))(c_7)
    
    c_8 = Add()([concatenate([c_4, c_4]), c_8])
    c_8 = ReLU()(c_8)
    c_8 = BatchNormalization()(c_8)
    
    c_9 =  TimeDistributed(Conv2D(filters=256, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu'))(c_8)
    c_10 = TimeDistributed(Conv2D(filters=256, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='linear'))(c_9)
    
    c_10 = Add()([c_8, c_10])
    c_10 = ReLU()(c_10)
    c_10 = BatchNormalization()(c_10)

    c_11 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu'))(c_10)
    c_12 = TimeDistributed(Conv2D(filters=1, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='linear'))(c_10)
    c_13 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='linear'))(c_10)
    c_14 = TimeDistributed(Conv2D(filters=1, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='linear'))(c_10)
    
    out = Add()([c_11, c_12, c_13, c_14])
    out = ReLU()(out)
    
    return out
    
    
def conv_lstm_inception(_in, weights):
    c_1 = ConvLSTM2D(filters=weights[0], kernel_size=(1, 1), strides=(1, 1),
                     padding='same', return_sequences=True, activation='tanh')(_in)
    
    c_3_reduce = ConvLSTM2D(filters=weights[1], kernel_size=(1, 1), strides=(1, 1),
                            padding='same', return_sequences=True, activation='tanh')(_in)
    c_3 = ConvLSTM2D(filters=weights[2], kernel_size=(3, 3), strides=(1, 1),
                     padding='same', return_sequences=True, activation='tanh')(c_3_reduce)
    
    c_5_reduce = ConvLSTM2D(filters=weights[3], kernel_size=(1, 1), strides=(1, 1),
                            padding='same', return_sequences=True, activation='tanh')(_in)
    c_5 = ConvLSTM2D(filters=weights[4], kernel_size=(5, 5), strides=(1, 1),
                     padding='same', return_sequences=True, activation='tanh')(c_5_reduce)
    
    # mp = TimeDistributed(MaxPooling2D(pool_size=(3, 3), strides=(1, 1),
    #                                   padding='same'))(_in)
    # c_1_mp = ConvLSTM2D(filters=weights[5], kernel_size=(1, 1), strides=(1, 1),
    #                     padding='same', return_sequences=True, activation='relu')(mp)
    
    return concatenate([c_1, c_3, c_5])
    
    
'''
def medium_3d_model(_in, name=''):
    
    kernel_init = 'glorot_uniform' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    # a_neurons = (64, 96, 128, 16, 32, 32)
    # b_neurons = (128, 128, 192, 32, 96, 64)
    # c_neurons = (192, 96, 208, 16, 48, 64)
    
    a_neurons = (np.array([64, 96, 128, 16, 32, 32]) * 1.5).astype(int) # 64 + 128 + 32 + 32 = 256
    b_neurons = (np.array([128, 128, 192, 32, 96, 64]) * 1.5).astype(int) # 128 + 192 + 96 + 64 = 480
    c_neurons = (np.array([192, 96, 208, 16, 48, 64]) * 1.5).astype(int) # 192 + 208 + 48 + 64 = 512
    
    _in_reduce = _5d_to_4d(_in)
    _in_reduce_norm = BatchNormalization()(_in_reduce)
    
    # a = inception_module(_in, *a_neurons, kernel_init, bias_init)
    a = inception_module(concatenate([_in_reduce, _in_reduce_norm]), *a_neurons, kernel_init, bias_init)
    a = BatchNormalization()(a)
    # b = inception_module(a, *b_neurons, kernel_init, bias_init)
    b = inception_module(a, *b_neurons, kernel_init, bias_init)
    b = BatchNormalization()(b)
    # c = inception_module(b, *c_neurons, kernel_init, bias_init)
    c = inception_module(b, *c_neurons, kernel_init, bias_init)
    c = BatchNormalization()(c)

    f_3 = Conv2D(filters=1024, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='relu', name='sub_categories_0')(concatenate([_in_reduce, c]))
    
    f_4 = Conv2D(filters=9, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='softmax', name='categorical')(f_3)

    return f_4
'''   

def simple_3d_model(_in, name=''):
    
    kernel_init = 'glorot_uniform' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    # a_neurons = (64, 96, 128, 16, 32, 32)
    # b_neurons = (128, 128, 192, 32, 96, 64)
    # c_neurons = (192, 96, 208, 16, 48, 64)
    
    # a_neurons = (np.array([64, 96, 128, 16, 32, 32]) * 1.5).astype(int)
    # b_neurons = (np.array([128, 128, 192, 32, 96, 64]) * 1.5).astype(int)
    # c_neurons = (np.array([192, 96, 208, 16, 48, 64]) * 1.5).astype(int)
    
    _in_reduce = _5d_to_4d(_in)
    
    # a = inception_module(_in, *a_neurons, kernel_init, bias_init)
    
    a = Conv2D(1024, (3, 3), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(_in_reduce)
    # a = Dropout(0.5)(a)
    b = Conv2D(1024, (3, 3), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(a)
    # b = Dropout(0.5)(b)
    c = Conv2D(1024, (3, 3), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(b)
    # c = Dropout(0.5)(c)
    d = Conv2D(1024, (3, 3), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(c)
    # d = Dropout(0.5)(d)
    # b = Conv2D(1024, (1, 1), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(a)
    # c = Conv2D(1024, (1, 1), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(b)

    # f_3 = Conv2D(filters=1024, kernel_size=(1, 1), strides=(1, 1),
    #              padding='same', activation='relu', name='sub_categories_0')(Add()([BatchNormalization()(_in_reduce), c]))
    
    # f_4 = CategoryEncoding(9, 'one_hot')(f_3)
    f_1 = Conv2D(filters=9, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='softmax', name='categorical_1')(Dropout(0.5)(a))
    f_2 = Conv2D(filters=9, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='softmax', name='categorical_2')(concatenate([Dropout(0.5)(b), f_1]))
    f_3 = Conv2D(filters=9, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='softmax', name='categorical_3')(concatenate([Dropout(0.5)(c), f_2]))
    f_4 = Conv2D(filters=9, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='softmax', name='categorical')(concatenate([Dropout(0.5)(d), f_3]))

    return f_1, f_2, f_3, f_4


def inception_module_aira(x,
                          _orig_shape,
                          kernel_init,
                          bias_init,
                          name=None):
    
    initial_depth = DepthwiseConv2D(kernel_size=(3, 3),
                                    strides=(1, 1),
                                    padding='same',
                                    activation='relu',
                                    kernel_initializer=kernel_init,
                                    bias_initializer=bias_init,
                                    kernel_regularizer='l1_l2',
                                    )(x)

    mp = MaxPooling2D(pool_size=(3, 3),
                      strides=(1, 1),
                      padding='same',
                      )(initial_depth)
    
    conv_1x1_mp = Conv2D(_orig_shape,
                         kernel_size=(1, 1),
                         strides=(1, 1),
                         padding='same',
                         activation='linear',
                         kernel_initializer=kernel_init,
                         bias_initializer=bias_init,
                         kernel_regularizer='l1_l2',
                         )(mp)
    
    conv_3x3 = SeparableConv2D(_orig_shape,
                               kernel_size=(3, 3),
                               strides=(1, 1),
                               depth_multiplier=4,
                               padding='same',
                               activation='linear',
                               kernel_initializer=kernel_init,
                               bias_initializer=bias_init,
                               kernel_regularizer='l1_l2',
                               )(initial_depth)

    conv_5x5 = SeparableConv2D(_orig_shape,
                               kernel_size=(5, 5),
                               strides=(1, 1),
                               depth_multiplier=8,
                               padding='same',
                               activation='linear',
                               kernel_initializer=kernel_init,
                               bias_initializer=bias_init,
                               kernel_regularizer='l1_l2',
                               )(initial_depth)
    
    output = Add()([x, conv_1x1_mp, conv_3x3, conv_5x5])
    output = ReLU()(output)
    
    return output
    
    
def inception_module(x,
                     filters_1x1,
                     filters_3x3_reduce,
                     filters_3x3,
                     filters_5x5_reduce,
                     filters_5x5,
                     filters_pool_proj,
                     kernel_init,
                     bias_init,
                     name=None):
    
    conv_1x1 = Conv2D(filters_1x1, (1, 1), padding='same', activation='relu',
                      kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    
    conv_3x3 = Conv2D(filters_3x3_reduce, (1, 1), padding='same', activation='relu',
                      kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    conv_3x3 = Conv2D(filters_3x3, (3, 3), padding='same', activation='relu',
                      kernel_initializer=kernel_init, bias_initializer=bias_init)(conv_3x3)

    conv_5x5 = Conv2D(filters_5x5_reduce, (1, 1), padding='same', activation='relu',
                      kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    conv_5x5 = Conv2D(filters_5x5, (5, 5), padding='same', activation='relu',
                      kernel_initializer=kernel_init, bias_initializer=bias_init)(conv_5x5)

    pool_proj = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(x)
    pool_proj = Conv2D(filters_pool_proj, (1, 1), padding='same', activation='relu',
                       kernel_initializer=kernel_init, bias_initializer=bias_init)(pool_proj)

    output = concatenate([conv_1x1, conv_3x3, conv_5x5, pool_proj])
    
    return output


def inception_module_c1(x,
                     filters_1x1,
                     filters_3x3_reduce,
                     filters_3x3,
                     filters_5x5_reduce,
                     filters_5x5,
                     filters_pool_proj,
                     kernel_init,
                     bias_init,
                     name=None):
    
    conv_1x1 = Conv2D(filters_1x1, (1, 1), padding='same', activation='linear',
                      kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    conv_1x1 = LeakyReLU(0.3)(conv_1x1)
    
    conv_3x3 = Conv2D(filters_3x3_reduce, (1, 1), padding='same', activation='linear',
                      kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    conv_3x3 = LeakyReLU(0.3)(conv_3x3)
    conv_3x3 = Conv2D(filters_3x3, (3, 3), padding='same', activation='linear',
                      kernel_initializer=kernel_init, bias_initializer=bias_init)(conv_3x3)
    conv_3x3 = LeakyReLU(0.3)(conv_3x3)

    conv_5x5 = Conv2D(filters_5x5_reduce, (1, 1), padding='same', activation='linear',
                      kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    conv_5x5 = LeakyReLU(0.3)(conv_5x5)
    conv_5x5 = Conv2D(filters_5x5, (5, 5), padding='same', activation='linear',
                      kernel_initializer=kernel_init, bias_initializer=bias_init)(conv_5x5)
    conv_5x5 = LeakyReLU(0.3)(conv_5x5)

    pool_proj = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(x)
    pool_proj = Conv2D(filters_pool_proj, (1, 1), padding='same', activation='linear',
                       kernel_initializer=kernel_init, bias_initializer=bias_init)(pool_proj)
    pool_proj = LeakyReLU(0.3)(pool_proj)

    pool_proj_min = Lambda(lambda x: -x)(x)
    pool_proj_min = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(pool_proj_min)
    pool_proj_min = Lambda(lambda x: -x)(pool_proj_min)
    pool_proj_min = Conv2D(filters_pool_proj, (1, 1), padding='same', activation='linear',
                       kernel_initializer=kernel_init, bias_initializer=bias_init)(pool_proj_min)
    pool_proj_min = LeakyReLU(0.3)(pool_proj_min)

    output = concatenate([conv_1x1, conv_3x3, conv_5x5, pool_proj, pool_proj_min])
    
    return output
    
    
def concat_reduce(x): # 5d to 4d
    shape = tf.shape(x)
    x_out = tf.reshape(x, [shape[0], shape[1], shape[2], shape[3] * shape[4]])
    return x_out
    
        
def _5d_to_4d(x):
    '''
    temporal feature extractor, needs at least 3 timesteps to work
    tries to extrapolate first and second derivatives in a ML way
    
    '''
    '''
    raw = Conv3D(filters=x.shape[-1], kernel_size=1, strides=1, padding='valid', activation='relu')(x)
    dt = Conv3D(filters=x.shape[-1], kernel_size=(2, 1, 1), strides=1, padding='valid', activation='relu')(x)
    d2t_from_raw = Conv3D(filters=x.shape[-1], kernel_size=(3, 1, 1), strides=1, padding='valid', activation='relu')(raw)
    d2t_from_dt = Conv3D(filters=x.shape[-1], kernel_size=(2, 1, 1), strides=1, padding='valid', activation='relu')(dt)

    concat = Concatenate(axis=1)([x, raw, dt, d2t_from_raw, d2t_from_dt])
    '''
    get_idx = lambda idx: Lambda(lambda x: x[:, idx: idx + 1, :, :, :], output_shape=(1, None, None, 145))
    
    a = get_idx(0)(x)
    b = get_idx(1)(x)
    c = get_idx(2)(x)
    
    dt_0 = Subtract()([b, a])
    dt_1 = Subtract()([c, b])
    d2t  = Subtract()([c, a])
    concat = Concatenate(axis=1)([b, dt_0, dt_1, d2t])
    perm = Permute((2, 3, 1, 4))(concat)
    output = Lambda(concat_reduce, output_shape=(None, None, perm.shape[-2] * perm.shape[-1]))(perm)
    
    return output
    
    
def get_vgg16(nx, ny, nz, loss=custom_loss, optimizer=None):
    model = Sequential()
    # Encoder
    # Block 1
    # model.add(BatchNormalization(axis=3, input_shape=(nx, ny, nz)))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu', name='block1_conv1', input_shape=(nx,ny,nz)))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu', name='block1_conv2'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool'))

    # Block 2
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu', name='block2_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu', name='block2_conv2'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool'))

    # Block 3
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', name='block3_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', name='block3_conv2'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', name='block3_conv3'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool'))

    # Block 4
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block4_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block4_conv2'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block4_conv3'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool'))


    # Block 5
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block5_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block5_conv2'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block5_conv3'))
    
    # model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool'))
    # intermission
    # model.add(BatchNormalization(axis=3))
    # model.add(Conv2D(4096, (1, 1), padding='same', activation='relu', name='block_inter_conv1'))
    # model.add(BatchNormalization(axis=3))
    # model.add(Conv2D(4096, (1, 1), padding='same', activation='relu', name='block_inter_conv2'))
    # model.add(BatchNormalization(axis=3))
    # model.add(Conv2D(1000, (1, 1), padding='same', activation='relu', name='block_inter_conv3'))
    
    # Decoder
    # Block 6
    model.add(UpSampling2D((2, 2), name='block6_upsampl'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block6_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block6_conv2'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block6_conv3'))
    
    # model.add(UpSampling2D((2, 2), name='block6.5_upsampl'))
    # model.add(BatchNormalization(axis=3))
    # model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block6.5_conv1'))
    # model.add(BatchNormalization(axis=3))
    # model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block6.5_conv2'))
    # model.add(BatchNormalization(axis=3))
    # model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block6.5_conv3'))

    # Block 7
    model.add(UpSampling2D((2, 2), name='block7_upsampl'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', name='block7_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', name='block7_conv2'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', name='block7_conv3'))

    # Block 8
    model.add(UpSampling2D((2, 2), name='block8_upsampl'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu', name='block8_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu', name='block8_conv2'))

    # Block 9
    model.add(UpSampling2D((2, 2), name='block9_upsampl'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu', name='block9_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu', name='block9_conv2'))

    # Output
    # model.add(BatchNormalization(axis=3))
    model.add(Conv2D(1, (1, 1), padding='same', activation='sigmoid', name='block10_conv1'))

    if optimizer is None:
        sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
        optimizer = sgd
    model.compile(loss=loss, optimizer=optimizer, metrics=[custom_mape, custom_acc, custom_acc_0, custom_acc_loss])
    
    print(model.summary())

    return model
    
    
def get_residual_vgg16(nx,ny,nz):
    # Encoder
    # Block 1
    _input = Input(shape=(nx, ny, nz))
    bn_0_0 = BatchNormalization(axis=3)(_input)
    conv2d_0_0 = Conv2D(64, (3, 3), padding='same', activation='linear', name='block1_conv1')(bn_0_0)
    relu_0_0 = ReLU(max_value=6)(conv2d_0_0)
    bn_0_1 = BatchNormalization(axis=3)(relu_0_0)
    conv2d_0_1 = Conv2D(64, (3, 3), padding='same', activation='linear', name='block1_conv2')(bn_0_1)
    resi_0 = Add()([conv2d_0_1, bn_0_1])
    relu_0_1 = ReLU(max_value=6)(resi_0)
    mp_0 = MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool')(relu_0_1)
    
    # Block 2
    bn_1_0 = BatchNormalization(axis=3)(mp_0)
    conv2d_1_0 = Conv2D(128, (3, 3), padding='same', activation='linear', name='block2_conv1')(bn_1_0)
    relu_1_0 = ReLU(max_value=6)(conv2d_1_0)
    bn_1_1 = BatchNormalization(axis=3)(relu_1_0)
    conv2d_1_1 = Conv2D(128, (3, 3), padding='same', activation='linear', name='block2_conv2')(bn_1_1)
    resi_1 = Add()([conv2d_1_1, bn_1_1])
    relu_1_1 = ReLU(max_value=6)(resi_1)
    mp_1 = MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool')(relu_1_1)

    # Block 3
    bn_2_0 = BatchNormalization(axis=3)(mp_1)
    conv2d_2_0 = Conv2D(256, (3, 3), padding='same', activation='linear', name='block3_conv1')(bn_2_0)
    relu_2_0 = ReLU(max_value=6)(conv2d_2_0)
    bn_2_1 = BatchNormalization(axis=3)(relu_2_0)
    conv2d_2_1 = Conv2D(256, (3, 3), padding='same', activation='linear', name='block3_conv2')(bn_2_1)
    resi_2 = Add()([conv2d_2_1, bn_2_1])
    relu_2_1 = ReLU(max_value=6)(resi_2)
    mp_2 = MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool')(relu_2_1)

    # Block 4
    bn_3_0 = BatchNormalization(axis=3)(mp_2)
    conv2d_3_0 = Conv2D(512, (3, 3), padding='same', activation='linear', name='block4_conv1')(bn_3_0)
    relu_3_0 = ReLU(max_value=6)(conv2d_3_0)
    bn_3_1 = BatchNormalization(axis=3)(relu_3_0)
    conv2d_3_1 = Conv2D(512, (3, 3), padding='same', activation='linear', name='block4_conv2')(bn_3_1)
    resi_3 = Add()([conv2d_3_1, bn_3_1])
    relu_3_1 = ReLU(max_value=6)(resi_3)
    mp_3 = MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool')(relu_3_1)


    # Block 5
    bn_4_0 = BatchNormalization(axis=3)(mp_3)
    conv2d_4_0 = Conv2D(512, (3, 3), padding='same', activation='linear', name='block5_conv1')(bn_4_0)
    relu_4_0 = ReLU(max_value=6)(conv2d_4_0)
    bn_4_1 = BatchNormalization(axis=3)(relu_4_0)
    conv2d_4_1 = Conv2D(512, (3, 3), padding='same', activation='linear', name='block5_conv2')(bn_4_1)
    resi_4 = Add()([conv2d_4_1, bn_4_1])
    relu_4_1 = ReLU(max_value=6)(resi_4)

    # Decoder
    # Block 6
    up_5 = UpSampling2D((2, 2), name='block6_upsampl')(relu_4_1)
    bn_5_0 = BatchNormalization(axis=3)(up_5)
    conv2d_5_0 = Conv2D(512, (3, 3), padding='same', activation='linear', name='block6_conv1')(bn_5_0)
    relu_5_0 = ReLU(max_value=6)(conv2d_5_0)
    bn_5_1 = BatchNormalization(axis=3)(relu_5_0)
    conv2d_5_1 = Conv2D(512, (3, 3), padding='same', activation='linear', name='block6_conv2')(bn_5_1)
    resi_5 = Add()([conv2d_5_1, bn_5_1])
    relu_5_1 = ReLU(max_value=6)(resi_5)

    # Block 7
    up_6 = UpSampling2D((2, 2), name='block7_upsampl')(relu_5_1)
    bn_6_0 = BatchNormalization(axis=3)(up_6)
    conv2d_6_0 = Conv2D(256, (3, 3), padding='same', activation='linear', name='block7_conv1')(bn_6_0)
    relu_6_0 = ReLU(max_value=6)(conv2d_6_0)
    bn_6_1 = BatchNormalization(axis=3)(relu_6_0)
    conv2d_6_1 = Conv2D(256, (3, 3), padding='same', activation='linear', name='block7_conv2')(bn_6_1)
    resi_6 = Add()([conv2d_6_1, bn_6_1])
    relu_6_1 = ReLU(max_value=6)(resi_6)

    # Block 8
    up_7 = UpSampling2D((2, 2), name='block8_upsampl')(relu_6_1)
    bn_7_0 = BatchNormalization(axis=3)(up_7)
    conv2d_7_0 = Conv2D(128, (3, 3), padding='same', activation='linear', name='block8_conv1')(bn_7_0)
    relu_7_0 = ReLU(max_value=6)(conv2d_7_0)
    bn_7_1 = BatchNormalization(axis=3)(relu_7_0)
    conv2d_7_1 = Conv2D(128, (3, 3), padding='same', activation='linear', name='block8_conv2')(bn_7_1)
    resi_7 = Add()([conv2d_7_1, bn_7_1])
    relu_7_1 = ReLU(max_value=6)(resi_7)

    # Block 9
    up_8 = UpSampling2D((2, 2), name='block9_upsampl')(relu_7_1)
    bn_8_0 = BatchNormalization(axis=3)(up_8)
    conv2d_8_0 = Conv2D(64, (3, 3), padding='same', activation='linear', name='block9_conv1')(bn_8_0)
    relu_8_0 = ReLU(max_value=6)(conv2d_8_0)
    bn_8_1 = BatchNormalization(axis=3)(relu_8_0)
    conv2d_8_1 = Conv2D(64, (3, 3), padding='same', activation='linear', name='block9_conv2')(bn_8_1)
    resi_8_1 = Add()([conv2d_8_1, bn_8_1])
    relu_8_1 = ReLU(max_value=6)(resi_8_1)

    # Output
    bn_9 = BatchNormalization(axis=3)(relu_8_1)
    conv2d_9_0 = Conv2D(1, (1, 1), padding='same', activation='relu', name='block10_conv1')(bn_9)

    model = Model(inputs=_input, outputs=conv2d_9_0)
    
    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True) 
    model.compile(loss='logcosh', optimizer=sgd, metrics=['mae', 'mse', custom_mape, custom_acc, custom_acc_0, custom_acc_loss])
    #model.compile(loss='mae', optimizer=Adam(lr=0.001), metrics=['mse'])
    print(model.summary())

    return model



'''    
def encode_block(_input, _filters, block_num):

    bn_0 = BatchNormalization(axis=3)(_input)
    conv2d_0 = Conv2D(filters=_filters, (3, 3), padding='same', activation='linear', name='block%d_conv1'%block_num)(bn_0)
    relu_0 = ReLU(max_value=6)(conv2d_0)
    bn_1 = BatchNormalization(axis=3)(relu_0)
    conv2d_1 = Conv2D(filters=_filters, (3, 3), padding='same', activation='linear', name='block%d_conv2'%block_num)(bn_1)
    resi = Add()([conv2d_1, bn_1])
    relu_1 = ReLU(max_value=6)(resi)
    mp = MaxPooling2D((2, 2), strides=(2, 2), name='block%d_pool'%block_num)(relu_1)
    return mp
    
    
def neutral_block():
    pass
def decode_block():
    pass
'''

'''
def maxpooling_mayhem_model(_in, name=''): # test later, promising
    
    kernel_init = 'random_normal' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    a = inception_module(_in,  64,  96, 128, 16, 32, 32, kernel_init, bias_init) # original
    # a = inception_module(_in,  128,  192, 256, 32, 64, 1, kernel_init, bias_init)
    a = BatchNormalization()(a) # original
    b = inception_module(a, 128, 128, 192, 32, 96, 64, kernel_init, bias_init) # original
    # b = inception_module(a, 256, 256, 384, 64, 192, 128, kernel_init, bias_init)
    b = BatchNormalization()(b) # original
    c = inception_module(b, 192,  96, 208, 16, 48, 64, kernel_init, bias_init) # original
    # c = inception_module(b, 384,  192, 416, 32, 96, 128, kernel_init, bias_init)
    c = BatchNormalization()(c) # original
    

    f_3 = Conv2D(filters=1024, kernel_size=(3, 3), strides=(1, 1),
                 padding='same', activation='relu')(c)
    f_3 = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(f_3)
    f_3 = Conv2D(filters=9 * 4, kernel_size=(3, 3), strides=(1, 1),
                 padding='same', activation='relu')(f_3)
    f_3 = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(f_3)
    f_3 = Conv2D(filters=9 * 2, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='relu')(f_3)
    f_3 = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(f_3)
    f_3 = Conv2D(filters=9, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='softmax')(f_3)
    
    return f_3    
'''
def aira2_c4_v1(_in, name=''):
    # .dot([0, 15, 30, 100])
    kernel_init = 'random_normal' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    a = inception_module(_in,  64,  96, 128, 16, 32, 32, kernel_init, bias_init)
    a = BatchNormalization()(a)
    b = inception_module(a, 128, 128, 192, 32, 96, 64, kernel_init, bias_init)
    b = BatchNormalization()(b)
    c = inception_module(b, 192,  96, 208, 16, 48, 64, kernel_init, bias_init)
    c = BatchNormalization()(c)
 
    f_3 = Conv2D(filters=1024, kernel_size=(3, 3), strides=(1, 1),
                 padding='same', activation='relu')(c)
    f_3 = BatchNormalization()(f_3)
    f_3 = Conv2D(filters=4, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='softmax')(f_3)
    return f_3
    
def aira2_c1(_in):
    # .dot([0, 15, 30, 100])
    kernel_init = 'random_normal' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    a = inception_module(_in,  64,  96, 128, 16, 32, 32, kernel_init, bias_init)
    a = BatchNormalization()(a)
    b = inception_module(a, 128, 128, 192, 32, 96, 64, kernel_init, bias_init)
    b = BatchNormalization()(b)
    c = inception_module(b, 192,  96, 208, 16, 48, 64, kernel_init, bias_init)
    c = BatchNormalization()(c)
 
    f_3 = Conv2D(filters=1024, kernel_size=(3, 3), strides=(1, 1),
                 padding='same', activation='relu')(c)
    f_3 = BatchNormalization()(f_3)
    f_3 = Conv2D(filters=1, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='exponential')(f_3)
    return f_3
'''
def aira2_c9_v1(_in, name=''):
    # loss = 1. + loss_scc - K.exp(-fp) * K.exp(-fn)
    # optimizer = 'nadam'
    kernel_init = 'random_normal'
    bias_init = initializers.Constant(value=0.2)
    
    a = inception_module(_in,  64,  96, 128, 16, 32, 32, kernel_init, bias_init)
    a = BatchNormalization()(a)
    b = inception_module(a, 128, 128, 192, 32, 96, 64, kernel_init, bias_init)
    b = BatchNormalization()(b)
    c = inception_module(b, 192,  96, 208, 16, 48, 64, kernel_init, bias_init)
    c = BatchNormalization()(c)
    f_3 = Conv2D(filters=256, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='linear', name='sub_categories_0')(c)
    f_3 = LeakyReLU(0.1)(f_3)
    f_4 = Conv2D(filters=9, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='softmax', name='categorical')(f_3)
    return f_4
'''