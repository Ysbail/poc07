import gc
import numpy as np
import netCDF4 as nc
import tensorflow as tf

import src.ncHelper as ncHelper
import src.grid_tricks as grid_tricks

from scipy.interpolate import RectBivariateSpline
from sklearn.model_selection import (
    train_test_split,
    StratifiedKFold,
)

np.random.seed(42)

class Dataset:
    def __init__(self, _input_path, _output_path, _dates, **kwargs):
        self.input_path = _input_path
        self.output_path = _output_path # str or list(str) or list(dict)
        self.dates = np.asanyarray(_dates)
        self._initial_idx = 10 # test size
        self._max_idx = self.dates.size
        self._idx = 0
        self._day_block_size = 'all' # train size
        
        self.norms = kwargs.get('norms', {})
        self.l_norms = kwargs.get('l_norms', {})
        self.l_bias = kwargs.get('l_bias', {})
        self._categories = kwargs.get('cat', []) # multiple categories, list of original cats
        self._num_multi_categories = len(self._categories)
        self._num_categories = len(self._categories)
        self._m_binary = kwargs.get('m_binary', [])
        self.__chunks_dim = kwargs.get('chunks_dim', 32)
        self.__timesteps = kwargs.get('timesteps', None)
        self.__diff = kwargs.get('diff', False)
        self.__stacking = kwargs.get('stacking', False)
        self._test_slice_head = kwargs.get('test_slice_head', True)
        
        self._input_slice_lats = None
        self._input_slice_lons = None
        self.general_lats = None
        self.general_lons = None
        self._range_lats = np.array([-90., 90.])
        self._range_lons = np.array([-180., 180.])
        self.lat_zone = slice(None)
        self.lon_zone = slice(None)
        
        self._multi_categories = [None] * self._num_multi_categories
        self._input_var_order = []
        self._per_variable_norm = {}
        self._output_var_order = []
        self._per_label_bias = {}
        self._per_label_norm = {}
        self.num_features = 0
        self.__num_features = 0
        self.num_labels = 0
        
        self._x_train = None
        self._y_train = None
        self._x_test = None
        self._y_test = None
        
        self.x_train = None
        self.y_train = None
        self.x_valid = None
        self.y_valid = None
        
        self.x_train_chunks = None
        self.y_train_chunks = None
        self.x_valid_chunks = None
        self.y_valid_chunks = None
        self.x_test_chunks = None
        self.y_test_chunks = None
        
        self.train_dates = None
        self.test_dates = None
        
        self.x_mean = 0.
        self.x_std = 0.
        self.y_mean = 0.
        self.y_std = 0.
        
        self.y_min = 0.
        self.y_ptp = kwargs.get('ptp', 100.)
        self.y_scale = kwargs.get('scale', 5.)
        self.y_threshold = 0.
        
        np.random.seed(42)
        self._eval()
        
        
    def _eval(self, ):
        _date = self.dates[0] # needs only the first date to check data shapes, assumes everything is the same
        
        _nc_input, _nc_output, _input_lats, _input_lons, _output_lats, _output_lons = self.load_data(_date)
        
        _input_res = np.max([np.diff(_input_lats).max(), np.diff(_input_lons).max()])
        _output_res = np.max([[np.diff(lats).max() for lats in _output_lats], [np.diff(lons).max() for lons in _output_lons]])
        
        print('resolutions: ', _input_res, _output_res, np.isclose(_input_res, _output_res))
        
        # keep input asis, down or up sample the output if needed and possible
        if _input_res >= _output_res:
            self._range_lats[0] = np.max([_input_lats.min(), *list(map(min, _output_lats))])
            self._range_lats[1] = np.min([_input_lats.max(), *list(map(max, _output_lats))])
            self._range_lons[0] = np.max([_input_lons.min(), *list(map(min, _output_lons))])
            self._range_lons[1] = np.min([_input_lons.max(), *list(map(max, _output_lons))])
            
            self._input_slice_lats = slice(np.argmin(abs(_input_lats - self._range_lats[0])),
                                           np.argmin(abs(_input_lats - self._range_lats[1])))
            self._input_slice_lons = slice(np.argmin(abs(_input_lons - self._range_lons[0])),
                                           np.argmin(abs(_input_lons - self._range_lons[1])))
            self.general_lats = _input_lats[self._input_slice_lats]
            self.general_lons = _input_lons[self._input_slice_lons]
            
        else:
            raise "input resolution is greater than the output's"
        
        # determine which variables to use and norms
        _vars = {k: 0 for k in _nc_input.variables.keys()}
        _dims = {k: 0 for k in _nc_input.dimensions.keys()}
        for k in _dims.keys(): _vars.pop(k) # remove the dimensions from variables
        self._input_var_order = _vars.keys()
        
        self._per_variable_norm = {k: 1. for k in _vars.keys()}
        
        self._output_var_order = []
        self._per_label_norm = []
        self._per_label_bias = []
        for _nc in _nc_output:
            _vars = {k: 0 for k in _nc.variables.keys()}
            _dims = {k: 0 for k in _nc.dimensions.keys()}
            for k in _dims.keys(): _vars.pop(k) # remove the dimensions from variables
            _vars.pop('nobs', None) # hard coded TODO
            self._output_var_order.append(_vars.keys())
            self._per_label_norm.append({k: 1. for k in _vars.keys()})
            self._per_label_bias.append({k: 0. for k in _vars.keys()})
        
        for k in self._input_var_order: # count number of features // does not have num of labels
            _shape = _nc_input.variables[k].shape
            if len(_shape) == 3:
                self.num_features += 1
            if len(_shape) == 4:
                self.num_features += _shape[1]
        
        self.__num_features = self.num_features
        
        self._per_variable_norm.update(self.norms)
        
        if isinstance(self.l_bias, dict):
            self._per_label_bias[0].update(self.l_bias)
        elif isinstance(self.l_bias, list): # list of dicts
            for _per_label_bias, bias in zip(self._per_label_bias, self.l_bias):
                _per_label_bias.update(bias)
        
        if isinstance(self.l_norms, dict):
            self._per_label_norm[0].update(self.l_norms)
        elif isinstance(self.l_norms, list): # list of dicts
            for _per_label_norm, norms in zip(self._per_label_norm, self.l_norms):
                _per_label_norm.update(norms)
        
        for _nc, _output_var_order in zip(_nc_output, self._output_var_order):
            for k in _output_var_order: # count number of features // does not have num of labels
                _shape = _nc.variables[k].shape
                if len(_shape) == 3:
                    self.num_labels += 1
                if len(_shape) == 4:
                    self.num_labels += _shape[1]
        
        '''
        _train_slice = slice(self._initial_idx, None) if self._test_slice_head else slice(None, -self._initial_idx)
        _test_slice = slice(None, self._initial_idx) if self._test_slice_head else slice(-self._initial_idx, None)
        
        self.train_dates = self.dates[_train_slice]
        np.random.shuffle(self.train_dates)
        self.test_dates = self.dates[_test_slice]
        '''
        
        _test = np.full(self.dates.size, False)
        _choice = np.random.choice(range(self.dates.size), size=self._initial_idx, replace=False)
        # _choice = np.array([9, 15, 68, 82, 97, 148, 163, 180, 183, 201]) # seed inconsistency temp fix
        _test[_choice] = True
        
        self.train_dates = self.dates[~_test]
        np.random.shuffle(self.train_dates)
        self.test_dates = self.dates[_test]
        print('test dates:', self.test_dates)
        
    
    def get_test(self, sample_size=None):
        # preprocess testing data    
        print('preprocessing test data')
        _x_test, _y_test = [], []
        
        if isinstance(sample_size, str) and sample_size.lower() == 'all':
            sample_size = None
        elif isinstance(sample_size, float):
            sample_size = int(sample_size)
            
        _slice = slice(sample_size)
        
        for _date in self.test_dates[_slice]:
            _x, _y = self.process_data(_date)

            _x_test.append(_x)
            _y_test.append(_y)
            
        _x_test = np.concatenate(_x_test).astype(np.float32)
        _y_test = np.concatenate(_y_test).astype(np.float32)
        
        '''
        self.x_min = np.nanmin(_x_test, axis=(1, 2), keepdims=True)
        self.x_ptp = np.ptp(_x_test, axis=(1, 2), keepdims=True)
        self.x_ptp[self.x_ptp == 0.] = 1.
        self.x_ptp[np.isnan(self.x_ptp)] = 1.
        
        _x_test = (_x_test - self.x_min) / self.x_ptp
        '''
        # self.y_ptp = 100.
        # self.y_min = 0.
        # self.y_scale = 5.
        # self.y_threshold = (1. - self.y_min) / self.y_ptp
        
        _y_test = (_y_test - self.y_min) / self.y_ptp
        # _thres = _y_test > self.y_threshold
        # _y_test[_thres] = (_y_test[_thres] - self.y_threshold) / self.y_scale + self.y_threshold
        
        if len(self._m_binary) > 1:
            _y_test = self._transform_m_binary(_y_test)
            _y_test = [*_y_test.reshape((*_y_test.shape[:-1], -1, 2)).transpose((3, 0, 1, 2, 4))]
        
        return _x_test, _y_test

      
    def rescale_y(self, y):
        _thres = y > self.y_threshold
        y[_thres] = (y[_thres] - self.y_threshold) * self.y_scale + self.y_threshold
        y = y * self.y_ptp + self.y_min
        
        return y
      
        
    @property
    def multi_categories(self):
        return self._multi_categories
        
        
    @multi_categories.setter
    def multi_categories(self, idx_array): # list (int, array)
        self._multi_categories[idx_array[0]] = idx_array[1]
        
    
    def _transform_m_binary(self, x):
        return np.concatenate([tf.one_hot(x[..., 0] >= (b / self.y_ptp), 2)
                               for b in self._m_binary], axis=-1)
        
        
    def _transform_categorical(self, attr):
        _temp_attr = getattr(self, attr)
        _rules = [(True if left is None else _temp_attr >= left) & (True if right is None else _temp_attr < right) for left, right in self._categories]
        for idx, _rule in enumerate(_rules):
            _temp_attr[_rule] = int(idx)
        _temp_attr = tf.one_hot(_temp_attr[..., 0], self._num_categories).numpy()
        print('_temp_attr shape', _temp_attr.shape)
        setattr(self, attr, _temp_attr)
    
    
    def _transform_multi_categorical(self, attr):
        for cat_idx, categories in enumerate(self._categories):
            _temp_attr = getattr(self, attr)
            _rules = [(True if left is None else _temp_attr >= left) & (True if right is None else _temp_attr < right) for left, right in categories]
            for idx, _rule in enumerate(_rules):
                _temp_attr[_rule] = int(idx)
            _temp_attr = tf.one_hot(_temp_attr[..., 0], self._num_categories).numpy()
            self.multi_categories = [cat_idx, _temp_attr]
        setattr(self, attr, self.multi_categories)
        
    
    def load_data(self, _date):
        _nc_input = nc.Dataset(self.input_path.format(date=_date), 'r', keepweakref=True)
        if isinstance(self.output_path, str):
            _nc_output = [nc.Dataset(self.output_path.format(date=_date), 'r', keepweakref=True)]
        elif isinstance(self.output_path, list):
            _nc_output = []
            for output in self.output_path:
                if isinstance(output, str):
                    _nc = nc.Dataset(output.format(date=_date), 'r', keepweakref=True)
                elif isinstance(output, dict):
                    _nc = nc.Dataset(*output['args'], **output['kwargs'](_date=_date))
                _nc_output.append(_nc)
            
        
        _input_lats, _input_lons = ncHelper.get_lats_lons_file(_nc_input, 1, 0)
        _output_lats, _output_lons = [], []
        for _nc in _nc_output:
            lats, lons = ncHelper.get_lats_lons_file(_nc, 1, 0)
            _output_lats.append(lats)
            _output_lons.append(lons)
            
        _output_lats = np.asanyarray(_output_lats, dtype=np.float32)
        _output_lons = np.asanyarray(_output_lons, dtype=np.float32)
        
        return _nc_input, _nc_output, _input_lats, _input_lons, _output_lats, _output_lons
    
    
    def process_data(self, _date, **kwargs):
        self.num_features = self.__num_features
        
        _nc_input, _nc_output, _input_lats, _input_lons, _output_lats, _output_lons = self.load_data(_date)
        
        _time_size = _nc_input['time'].size
        _x = np.empty((_time_size, self.num_features, self.general_lats.size, self.general_lons.size))
        _y = np.empty((_time_size, self.num_labels, self.general_lats.size, self.general_lons.size))
        
        _aux_idx = np.arange(_y.shape[0])
        # concat over time & cut/reshape
        _curr_index = 0
        # save some spacing
        _i = slice(None, _time_size)
        _j = self._input_slice_lats
        _k = self._input_slice_lons
        _lats = self.general_lats.size
        _lons = self.general_lons.size
        
        self._var_header = {}
        for k in self._input_var_order:
            _var = _nc_input.variables[k]
            _shape = _var.shape
            self._var_header[k] = _curr_index
            if len(_shape) == 3:
                if k in ['apcpsfc', 'acpcpsfc']:
                    _var = np.r_[_var[:1], _var[1:] - _var[:-1]] # gambi
                _x[:, slice(_curr_index, _curr_index + 1), :, :] = _var[_i, _j, _k].reshape(_time_size, -1, _lats, _lons) / self._per_variable_norm[k]
                _curr_index += 1
            elif len(_shape) == 4:
                _x[:, slice(_curr_index, _curr_index + _shape[1]), :, :] = _var[_i, :, _j, _k].reshape(_time_size, -1, _lats, _lons) / self._per_variable_norm[k]
                _curr_index += _shape[1]
                
        _curr_index = 0 
        for _nc, _output_var_order, _per_label_norm, _per_label_bias in zip(_nc_output, self._output_var_order, self._per_label_norm, self._per_label_bias):
            for k in _output_var_order:
                _var = _nc.variables[k]
                if 'latitude' in _nc.variables.keys():
                    _rect_func = lambda grid: RectBivariateSpline(_nc['latitude'][:], _nc['longitude'][:], grid)
                else:
                    _rect_func = lambda grid: RectBivariateSpline(_nc['lat'][:], _nc['lon'][:], grid)
                _shape = _var.shape
                if len(_shape) == 3:
                    _temp_y = _var[_i, :, :]
                    if _temp_y.shape[0] == 1:
                        _y[:, slice(_curr_index, _curr_index + 1), :, :] = _rect_func(_temp_y[0])(self.general_lats, self.general_lons)[None].repeat(_time_size, axis=0)[:, None]
                    else:
                        _y[:, slice(_curr_index, _curr_index + 1), :, :] = np.reshape([_rect_func(_t_y)(self.general_lats, self.general_lons) for _t_y in _temp_y], (_time_size, -1, _lats, _lons))
                    _curr_index += 1
                elif len(_shape) == 4:
                    _temp_y = _var[_i, :, :, :]
                    _y[:, slice(_curr_index, _curr_index + _shape[1]), :, :] = np.reshape([_rect_func(_t_y)(self.general_lats, self.general_lons) for _t_levels_y in temp_y for _t_y in _t_levels_y], (_time_size, -1, _lats, _lons))
                    _curr_index += _shape[1]
                
        
        # transpose & crop
        _x = _x.transpose((0, 2, 3, 1)).astype(np.float32) # [:, self.lat_zone, self.lon_zone, :]
        _y = _y.transpose((0, 2, 3, 1)).astype(np.float32) # [:, self.lat_zone, self.lon_zone, :]
        
        # _y_mask = _y < 0.
        # _y = np.ma.array(_y, mask=_y_mask)
        
        # _y = _y.clip(0., None)
        
        # fix winds
        
        # levels - get slices
        wind_level_spd = 22 * 4
        wind_level_dir = 22 * 5
        wind_sfc_spd = 22 * 6 + 5
        wind_sfc_dir = 22 * 6 + 4
                
        _slice_wind_level_spd = slice(wind_level_spd, wind_level_spd + 22)
        _slice_wind_level_dir = slice(wind_level_dir, wind_level_dir + 22)
        
        # sfc - get indices
        _slice_wind_sfc_spd = slice(wind_sfc_spd, wind_sfc_spd + 1)
        _slice_wind_sfc_dir = slice(wind_sfc_dir, wind_sfc_dir + 1)
        
        _u = _x[..., _slice_wind_level_spd] * np.cos(- np.pi / 2. - np.radians(_x[..., _slice_wind_level_dir]))
        _v = _x[..., _slice_wind_level_spd] * np.sin(- np.pi / 2. - np.radians(_x[..., _slice_wind_level_dir]))
        
        _x[..., _slice_wind_level_spd] = _u
        _x[..., _slice_wind_level_dir] = _v
        
        _u = _x[..., _slice_wind_sfc_spd] * np.cos(- np.pi / 2. - np.radians(_x[..., _slice_wind_sfc_dir]))
        _v = _x[..., _slice_wind_sfc_spd] * np.sin(- np.pi / 2. - np.radians(_x[..., _slice_wind_sfc_dir]))
        
        _x[..., _slice_wind_sfc_dir] = _u
        _x[..., _slice_wind_sfc_spd] = _v
        
        _x_day_cos = np.cos(np.arange(24) * 2 * np.pi / 24)[:, None, None, None].repeat(_x.shape[1], axis=1).repeat(_x.shape[2], axis=2)
        _x_day_sin = np.sin(np.arange(24) * 2 * np.pi / 24)[:, None, None, None].repeat(_x.shape[1], axis=1).repeat(_x.shape[2], axis=2)
        
        _x = np.concatenate((_x, _x_day_cos, _x_day_sin), axis=-1)
        
        self.num_features += 2
        
        if self.__stacking:
            _x = np.array([np.concatenate(_x[i: i + 2], axis=-1) for i in range(0, _x.shape[0] - 1)])
            _y = _y[:-1]
            
            self.num_features *= 2
        
        print('shapes:', _x.shape, _y.shape)
        
        return _x, _y
        
        
    def _prepare_curr_dataset(self, ):
        self.x_train = None
        self.y_train = None
        self.x_valid = None
        self.y_valid = None
        gc.collect()
    
        if self._day_block_size.lower() == 'all':
            _slice = tuple(np.arange(4) * 6 + int(self._idx // 2))
            print('building %dth half train data from the hours:'%(self._idx % 2 + 1), _slice)
            _x, _y = [], []
            for _date in self.train_dates[(self._idx % 2)::2]:
                _temp_x, _temp_y = self.process_data(_date)
                _x.append(_temp_x[_slice, ])
                _y.append(_temp_y[_slice, ])
        else:
            _date = self.train_dates[self._idx] # ?
            _x, _y = [], []
            if (self._idx + self._day_block_size) <= self.train_dates.size:
                print('building %d days of train data'%self._day_block_size)
                for i in range(self._day_block_size):
                    _temp_x, _temp_y = self.process_data(self.train_dates[self._idx + i])
                    _x.append(_temp_x)
                    _y.append(_temp_y)
            else:
                raise StopIteration
                # return

        _x = np.concatenate(_x, axis=0)
        _y = np.concatenate(_y, axis=0)
        print('_x:',_x.shape)
        print('_y:',_y.shape)

        # self.y_ptp = 100.
        # self.y_min = 0.
        # self.y_scale = 5.
        # self.y_threshold = (1. - self.y_min) / self.y_ptp
        # 
        _y = (_y - self.y_min) / self.y_ptp
        _y = _y.clip(0., 1.)
        # _thres = self.y_train > self.y_threshold
        # self.y_train[_thres] = (self.y_train[_thres] - self.y_threshold) / self.y_scale + self.y_threshold
        # self.y_train[self.y_train < 0.005] = 0.

        if len(self._m_binary) > 1:
            _y = self._transform_m_binary(_y)

        self.x_train, self.y_train = grid_tricks.get_chunks(_x, _y, target_dim=(32, 32))
        
        x_train_add, y_train_add = grid_tricks.get_chunks(_x[:, 16:, :], _y[:, 16:, :], target_dim=(32, 32))
        self.x_train = np.r_[self.x_train, x_train_add]
        self.y_train = np.r_[self.y_train, y_train_add]
        
        x_train_add, y_train_add = grid_tricks.get_chunks(_x[:, :, 16:], _y[:, :, 16:], target_dim=(32, 32))
        self.x_train = np.r_[self.x_train, x_train_add]
        self.y_train = np.r_[self.y_train, y_train_add]
        
        x_train_add, y_train_add = grid_tricks.get_chunks(_x[:, 16:, 16:], _y[:, 16:, 16:], target_dim=(32, 32))
        self.x_train = np.r_[self.x_train, x_train_add]
        self.y_train = np.r_[self.y_train, y_train_add]
        
        del(_x)
        del(_y)
        gc.collect()
        
        # getting 'more' samples with rain
        print('total samples:', self.y_train.shape[0])
        self.x_train, self.x_valid, self.y_train, self.y_valid = train_test_split(self.x_train, self.y_train, test_size=0.2)

        if len(self._m_binary) > 1:
            self.y_train = [*self.y_train.reshape((*self.y_train.shape[:-1], -1, 2)).transpose((3, 0, 1, 2, 4))]
            self.y_valid = [*self.y_valid.reshape((*self.y_valid.shape[:-1], -1, 2)).transpose((3, 0, 1, 2, 4))]
            
            
    def iterdates(self, lat_zone=slice(None), lon_zone=slice(None)):
        # self.lat_zone = lat_zone
        # self.lon_zone = lon_zone
        return iter(self)
    
    def __iter__(self, ):
        self._idx = 0
        return self
        
    def __next__(self, ):
        if self._day_block_size.lower() == 'all':
            if self._idx < 12:
                self._prepare_curr_dataset()
                self._idx += 1
                return None, self
            else:
                raise StopIteration
        elif self._idx < self._max_idx - self._initial_idx:
            _date = self.train_dates[self._idx]
            self._prepare_curr_dataset()
            self._idx += self._day_block_size
            return _date, self
        else:
            raise StopIteration