import numpy as np
import tensorflow as tf
import tensorflow.keras.backend as K
from keras.losses import (
    BinaryCrossentropy,
    SparseCategoricalCrossentropy,
    categorical_crossentropy,
    LogCosh,
    MeanSquaredError,
    MeanAbsoluteError,
    MeanAbsolutePercentageError,
)

_classes = 16

def custom_loss(y_true, y_pred):
    # loss = 1. - K.mean(K.exp(-K.pow(y_true - y_pred, 2)))
    
    # loss_mape = K.mean(K.abs(y_true - y_pred) / K.clip(y_true, K.epsilon(), None))
    # loss_mae = K.mean(K.abs(y_true - y_pred))
    # loss_mse = K.mean(K.pow(y_true - y_pred, 2))
    # fn = false_negative(y_true, y_pred)
    # fp = false_positive(y_true, y_pred)
    # loss_scc = SparseCategoricalCrossentropy()(y_true, y_pred)
    # loss_bc = BinaryCrossentropy()(y_true, y_pred)
    # fnr = cat_false_negative(y_true, y_pred)
    # fpr = cat_false_positive(y_true, y_pred)
    
    return _custom_categorical_crossentropy(y_true, y_pred)


_epsilon = K.constant(K.epsilon(), 'float32')
def _custom_categorical_crossentropy(y_true, y_pred):
    y_true = K.cast(y_true, 'float32')
    y_pred = K.cast(y_pred, 'float32')

    w_sum = K.sum(y_true, axis=list(range(1, y_true.shape.ndims - 1)))
    w_sum = tf.clip_by_value(w_sum / K.sum(w_sum, axis=-1, keepdims=True), _epsilon, 1. - _epsilon)
    w_ent = -K.log(w_sum)[:, None, None, None, :]

    y_pred = tf.clip_by_value(y_pred, _epsilon, 1. - _epsilon)
    y_pred = -K.log(y_pred)
    
    return K.mean(y_pred * y_true * w_ent, axis=-1)


_epsilon = K.constant(K.epsilon(), 'float32')
def _custom_sim_categorical_crossentropy(y_true, y_pred):
    y_true = K.cast(y_true, 'float32')
    y_pred = K.cast(y_pred, 'float32')
    
    l = tf.clip_by_value(K.argmax(y_true, axis=-1) - 1, 0, 15)
    r = tf.clip_by_value(K.argmax(y_true, axis=-1) + 1, 0, 15)
    
    y_true = tf.clip_by_value(y_true + K.cast(tf.one_hot(l, 16), 'float32') * 0.5 + K.cast(tf.one_hot(r, 16), 'float32') * 0.5, 0., 1.)
    
    w_sum = K.sum(y_true, axis=list(range(1, y_true.shape.ndims - 1)))
    w_sum = tf.clip_by_value(w_sum / K.sum(w_sum, axis=-1, keepdims=True), _epsilon, 1. - _epsilon)
    w_ent = -K.log(w_sum)[:, None, None, :]

    y_pred = tf.clip_by_value(y_pred, _epsilon, 1. - _epsilon)
    y_pred = -K.log(y_pred)
    
    return K.sum(y_pred * y_true * w_ent, axis=-1)
    

def _custom_categorical_absolute(y_true, y_pred):
    y_true = K.cast(y_true, 'float32')
    y_pred = K.cast(y_pred, 'float32')
    
    beta = 1. - K.sum(y_true, axis=(0, 1, 2), keepdims=True) / tf.math.maximum(K.sum(y_true, keepdims=True), K.epsilon()) # (None, 1, 1, 16)
    return K.sum(beta * K.mean(- y_true * K.log(y_pred), axis=(1, 2), keepdims=True), axis=-1, keepdims=True)
    
    '''
    w_sum = K.sum(y_true, axis=list(range(1, y_true.shape.ndims - 1)))
    w_sum = tf.clip_by_value(w_sum / K.sum(w_sum, axis=-1, keepdims=True), _epsilon, 1. - _epsilon)
    w_ent = -K.log(w_sum)[:, None, None, :]

    y_pred = tf.clip_by_value(y_pred, _epsilon, 1. - _epsilon)
    y_pred = -K.log(y_pred)
    
    return K.sum(y_pred * y_true * w_ent, axis=-1)
    '''
    
def custom_mape(y_true, y_pred):
    loss = K.mean(K.abs(y_true - y_pred) / K.clip(K.abs(y_true), K.epsilon(), None))
    # print('custom_loss:', loss)
    return loss

    
def custom_acc(y_true, y_pred):
    score = K.sum(K.cast(K.less_equal(K.abs(y_true - y_pred), 0.0001 + K.abs(y_true * 0.05)), y_true.dtype) * K.cast(K.greater(y_true, 0.001), y_true.dtype) * K.cast(K.greater(y_pred, 0.001), y_true.dtype)) / (1. + K.sum(K.cast(K.greater(y_true, 0.001), y_true.dtype)))
    return score

    
def custom_acc_0(y_true, y_pred):
    score = K.sum(K.cast(K.less_equal(y_true, 0.001), y_true.dtype) * K.cast(K.less_equal(y_pred, 0.001), y_true.dtype)) / (1. + K.sum(K.cast(K.less_equal(y_true, 0.001), y_true.dtype)))
    return score

    
def custom_acc_loss(y_true, y_pred):
    loss = 1. - K.sqrt(custom_acc(y_true, y_pred) * custom_acc_0(y_true, y_pred))
    return loss

    
def custom_dist(y_true, y_pred):
    loss = K.mean(K.square(K.pow(y_true, 2) - K.pow(y_pred, 2)))
    return loss

    
def false_positive(y_true, y_pred):
    # any 1+ class that should be 0
    t = K.reshape(y_true, (-1, 1))
    p = K.reshape(K.argmax(y_pred, axis=-1), (-1, 1))
    TN = K.sum(K.cast(K.equal(t, 0), y_true.dtype) * K.cast(K.equal(p, 0), y_true.dtype))
    FP = K.sum(K.cast(K.equal(t, 0), y_true.dtype) * K.cast(K.greater(p, 0), y_true.dtype))
    return 1. - TN / (TN + FP)


def cat_false_positive(y_true, y_pred):
    # any 1+ class that should be 0
    t = K.reshape(K.argmax(y_true, axis=-1), (-1, 1))
    p = K.reshape(K.argmax(y_pred, axis=-1), (-1, 1))
    TN = K.sum(K.cast(K.equal(t, 0), 'float32') * K.cast(K.equal(p, 0), 'float32'))
    FP = K.sum(K.cast(K.equal(t, 0), 'float32') * K.cast(K.greater(p, 0), 'float32'))
    return 1. - TN / tf.math.maximum(TN + FP, K.epsilon())
    
    
def cat_false_negative(y_true, y_pred):
    _classes = 7
    t = K.reshape(K.argmax(y_true, axis=-1), (-1, 1))
    p = K.reshape(K.argmax(y_pred, axis=-1), (-1, 1))
    TP = K.sum(K.cast([K.sum(K.cast(K.equal(t, c), 'float32') * K.cast(K.equal(p, c), 'float32')) for c in range(1, _classes)], 'float32'))
    FN = K.sum(K.cast(K.greater(t, 0), 'float32') * K.cast(K.equal(p, 0), 'float32'))
    return 1. - TP / tf.math.maximum(TP + FN, K.epsilon())


def less_mistakes(y_true, y_pred):
    fnr = cat_false_negative(y_true, y_pred)
    fpr = cat_false_positive(y_true, y_pred)
    return (fpr + 1) * (fnr + 1)
    

def accuracy_entropy(y_true, y_pred):
    t = K.reshape(y_true, (-1, 1))
    p = K.reshape(K.argmax(y_pred, axis=-1), (-1, 1))
    
    cats = K.cast([K.mean(K.cast(K.equal(t, c), 'float32')) for c in range(_classes)], 'float32')
    accs = K.cast([K.sum(K.cast(K.equal(t, c), 'float32') * K.cast(K.equal(p, c), 'float32')) / (K.sum(K.cast(K.equal(t, c), 'float32')) + K.epsilon()) for c in range(_classes)], 'float32')

    entr = -K.log(cats)
    entr = K.minimum(entr, 7.)
    
    acc = K.sum(accs * entr) / K.sum(entr)

    return acc


def cat_accuracy_entropy(y_true, y_pred):
    # t = K.reshape(K.argmax(y_true, axis=-1), (-1, 1))
    # p = K.reshape(K.argmax(y_pred, axis=-1), (-1, 1))
    
    # cats = K.cast([K.mean(K.cast(K.equal(t, c), 'float32')) for c in range(_classes)], 'float32')
    # accs = K.cast([K.sum(K.cast(K.equal(t, c), 'float32') * K.cast(K.equal(p, c), 'float32')) / (K.sum(K.cast(K.equal(t, c), 'float32')) + K.epsilon()) for c in range(_classes)], 'float32')

    # entr = -K.log(cats)
    # entr = K.minimum(entr, 7.)
    
    # acc = K.sum(accs * entr) / K.sum(entr)
    p = K.argmax(y_pred, axis=-1) # balanced accuracy
    t = K.argmax(y_true, axis=-1)
    acc = K.mean(K.mean(K.cast(tf.one_hot(p, 7), 'float32') * K.cast(tf.one_hot(t, 7), 'float32'), axis=(0, 1, 2)))

    return acc
    

def cat_accuracy_entropy_score(y_true, y_pred):
    fpr = cat_false_positive(y_true, y_pred)
    fnr = cat_false_negative(y_true, y_pred)
    exp_fpr = K.exp(-fpr)
    exp_fnr = K.exp(-fnr)
    exp_fpr = tf.where(K.greater(exp_fpr, 0.6), exp_fpr, 0.01)
    exp_fnr = tf.where(K.greater(exp_fnr, 0.6), exp_fpr, 0.01)
    return cat_accuracy_entropy(y_true, y_pred) * exp_fpr * exp_fnr
      
    
def acc_entr_loss(y_true, y_pred):
    return 1. - accuracy_entropy(y_true, y_pred)


def false_negative(y_true, y_pred):
    t = K.reshape(y_true, (-1, 1))
    p = K.reshape(K.argmax(y_pred, axis=-1), (-1, 1))
    TP = K.sum(K.cast([K.sum(K.cast(K.equal(t, c), 'float32') * K.cast(K.equal(p, c), 'float32')) for c in range(1, _classes)], 'float32'))
    FN = K.sum(K.cast(K.greater(t, 0), y_true.dtype) * K.cast(K.equal(p, 0), y_true.dtype))
    return 1. - TP / (TP + FN)


def dunno_loss(y_true, y_pred):
    FPR = false_positive(y_true, y_pred)
    FNR = false_negative(y_true, y_pred)
    SCC = SparseCategoricalCrossentropy()(y_true, y_pred)
    return FPR + FNR + SCC
    
    
def prop_false_negative(y_true, y_pred):
    # any 0 class that should be 1+
    t = K.reshape(y_true, (-1, 1))
    p = K.reshape(K.argmax(y_pred, axis=-1), (-1, 1))
    return K.sum(K.cast(K.greater(t, 0), y_true.dtype) * K.cast(K.equal(p, 0), y_true.dtype)) / K.sum(K.cast(K.greater(t, 0), y_true.dtype))
    
    
def above_1_acc(y_true, y_pred):
    true = K.cast(K.greater_equal(y_true, 0.01), y_true.dtype)
    pred = K.cast(K.greater_equal(y_pred, 0.01), y_true.dtype)
    acc = K.sum(true * pred) / K.sum(true)
    return acc
    
    
def below_1_acc(y_true, y_pred):
    true = K.cast(K.less(y_true, 0.01), y_true.dtype)
    pred = K.cast(K.less(y_pred, 0.01), y_true.dtype)
    acc = K.sum(true * pred) / K.sum(true)
    return acc


def above_10_acc(y_true, y_pred):
    true = K.cast(K.greater_equal(y_true, 0.1), y_true.dtype)
    pred = K.cast(K.greater_equal(y_pred, 0.1), y_true.dtype)
    acc = K.sum(true * pred) / K.sum(true)
    return acc
    
    
def below_10_acc(y_true, y_pred):
    true = K.cast(K.less(y_true, 0.1), y_true.dtype)
    pred = K.cast(K.less(y_pred, 0.1), y_true.dtype)
    acc = K.sum(true * pred) / K.sum(true)
    return acc
    

def gen_aira_f1_1mm_score(norm=100., scale=5.):
    _1mm_scaled = (1. / norm - 0.01) / scale + 0.01
    def aira_f1_1mm_score(y_true, y_pred):
        true = K.cast(K.greater_equal(y_true, _1mm_scaled), y_true.dtype)
        false = K.cast(K.less(y_true, _1mm_scaled), y_true.dtype)
        pred_true = K.cast(K.greater_equal(y_pred, _1mm_scaled), y_true.dtype)
        pred_false = K.cast(K.less(y_pred, _1mm_scaled), y_true.dtype)
        
        tp = K.sum(pred_true * true)
        fp = K.sum(pred_true * false)
        fn = K.sum(pred_false * true)
        
        precision = tp / K.maximum((tp + fp), 1.)
        recall = tp / K.maximum((tp + fn), 1.)
        
        f1 = 2 * precision * recall / K.maximum((precision + recall), 1.)
        
        return f1
        
    return aira_f1_1mm_score


def gen_aira_f1_5mm_score(norm=100., scale=5.):
    _5mm_scaled = (5. / norm - 0.01) / scale + 0.01
    def aira_f1_5mm_score(y_true, y_pred):
        true = K.cast(K.greater_equal(y_true, _5mm_scaled), y_true.dtype)
        false = K.cast(K.less(y_true, _5mm_scaled), y_true.dtype)
        pred_true = K.cast(K.greater_equal(y_pred, _5mm_scaled), y_true.dtype)
        pred_false = K.cast(K.less(y_pred, _5mm_scaled), y_true.dtype)
        
        tp = K.sum(pred_true * true)
        fp = K.sum(pred_true * false)
        fn = K.sum(pred_false * true)
        
        precision = tp / K.maximum((tp + fp), 1.)
        recall = tp / K.maximum((tp + fn), 1.)
        
        f1 = 2 * precision * recall / K.maximum((precision + recall), 1.)
        
        return f1
        
    return aira_f1_5mm_score
    
    
def gen_aira_f1_10mm_score(norm=100., scale=5.):
    _10mm_scaled = (10. / norm - 0.01) / scale + 0.01
    def aira_f1_10mm_score(y_true, y_pred):
        true = K.cast(K.greater_equal(y_true, _10mm_scaled), y_true.dtype)
        false = K.cast(K.less(y_true, _10mm_scaled), y_true.dtype)
        pred_true = K.cast(K.greater_equal(y_pred, _10mm_scaled), y_true.dtype)
        pred_false = K.cast(K.less(y_pred, _10mm_scaled), y_true.dtype)
        
        tp = K.sum(pred_true * true)
        fp = K.sum(pred_true * false)
        fn = K.sum(pred_false * true)
        
        precision = tp / K.maximum((tp + fp), 1.)
        recall = tp / K.maximum((tp + fn), 1.)
        
        f1 = 2 * precision * recall / K.maximum((precision + recall), 1.)
        
        return f1
        
    return aira_f1_10mm_score


def gen_aira_f1_15mm_score(norm=100., scale=5.):
    _15mm_scaled = (15. / norm - 0.01) / scale + 0.01
    def aira_f1_15mm_score(y_true, y_pred):
        true = K.cast(K.greater_equal(y_true, _15mm_scaled), y_true.dtype)
        false = K.cast(K.less(y_true, _15mm_scaled), y_true.dtype)
        pred_true = K.cast(K.greater_equal(y_pred, _15mm_scaled), y_true.dtype)
        pred_false = K.cast(K.less(y_pred, _15mm_scaled), y_true.dtype)
        
        tp = K.sum(pred_true * true)
        fp = K.sum(pred_true * false)
        fn = K.sum(pred_false * true)
        
        precision = tp / K.maximum((tp + fp), 1.)
        recall = tp / K.maximum((tp + fn), 1.)
        
        f1 = 2 * precision * recall / K.maximum((precision + recall), 1.)
        
        return f1
        
    return aira_f1_15mm_score
    
    
_epsilon = K.constant(K.epsilon(), 'float32')
def gen_aira_f1_score(**kwargs):
    def aira_f1_score(y_true, y_pred):
        f_1 = K.maximum(gen_aira_f1_1mm_score(**kwargs)(y_true, y_pred), _epsilon)
        f_5 = K.maximum(gen_aira_f1_5mm_score(**kwargs)(y_true, y_pred), _epsilon)
        f_10 = K.maximum(gen_aira_f1_10mm_score(**kwargs)(y_true, y_pred), _epsilon)
        f_15 = K.maximum(gen_aira_f1_15mm_score(**kwargs)(y_true, y_pred), _epsilon)
        
        return f_1 * f_5 * f_10 * f_15
        
    return aira_f1_score
    

def gen_precision_1_acc(norm=100., scale=5.):
    _1mm_scaled = (1. / norm - 0.01) / scale + 0.01
    def precision_1(y_true, y_pred):
        true = K.cast(K.greater_equal(y_true, _1mm_scaled), y_true.dtype)
        false = K.cast(K.less(y_true, _1mm_scaled), y_true.dtype)
        pred_true = K.cast(K.greater_equal(y_pred, _1mm_scaled), y_true.dtype)
        
        tp = K.sum(pred_true * true)
        fp = K.sum(pred_true * false)
        
        precision = tp / K.maximum((tp + fp), 1.)
        
        return precision
        
    return precision_1
    
    
def gen_recall_1_acc(norm=100., scale=5.):
    _1mm_scaled = (1. / norm - 0.01) / scale + 0.01
    def recall_1(y_true, y_pred):
        true = K.cast(K.greater_equal(y_true, _1mm_scaled), y_true.dtype)
        pred_true = K.cast(K.greater_equal(y_pred, _1mm_scaled), y_true.dtype)
        pred_false = K.cast(K.less(y_pred, _1mm_scaled), y_true.dtype)
        
        tp = K.sum(pred_true * true)
        fn = K.sum(pred_false * true)

        recall = tp / K.maximum((tp + fn), 1.)
        
        return recall
        
    return recall_1
        
   
def below_above_score(y_true, y_pred):
    below = below_1_acc(y_true, y_pred)
    above = above_1_acc(y_true, y_pred)
    
    bas = 2 * below * above / (below + above) 
    
    return bas
    
    
def aira_score(y_true, y_pred):
    f1 = K.cast(aira_f1_score(y_true, y_pred), 'float32')
    bas = K.cast(below_above_score(y_true, y_pred), 'float32')
    
    return K.maximum(f1, bas)