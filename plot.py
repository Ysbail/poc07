import requests
import itertools
import pandas as pd
import numpy as np
import tensorflow as tf
import datetime as dt
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.use("Agg")

import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

import glob

import src.models as models
import src.grid_tricks as grid_tricks
from src.dataset import Dataset
from tensorflow.keras.models import load_model
from src.customs import (
    custom_loss,
    custom_mape,
    custom_acc,
    custom_acc_0,
    custom_acc_loss,
    false_negative,
    false_positive,
    accuracy_entropy,
    cat_false_negative,
    cat_false_positive,
    cat_accuracy_entropy,
    cat_accuracy_entropy_score,
)

dtNow = dt.datetime.utcnow()

input_path = '/storagefinep/wrf/dataset/{date:%Y%m%d}_wrf_dataset.nc'
output_path = '/storagefinep/merge/dataset/{date:%Y%m%d}_merge_dataset.nc'
tmax_url = 'http://ftp.cptec.inpe.br/modelos/tempo/SAMeT/DAILY/TMAX/%Y/%m/SAMeT_CPTEC_TMAX_%Y%m%d.nc'
output_path_2 = {'args': ('inmemory.nc', ),
                 'kwargs': lambda _date: {'memory': requests.get(_date.strftime(tmax_url)).content}, 
                }

date_ini = '2021-01-01'
date_fin = '2021-07-25'

dates = pd.date_range(date_ini, date_fin, freq='D').to_pydatetime()
suffix = 'poc07'
# fmodel = './models/dafuq.h5'
fmodel = sorted(glob.glob(f'./models/bkp_*_aira2_{suffix}.h5'))[-1]
_scale = 1.
_ptp = 100.
# fmodel = './models/best_20210701.h5'
# fmodel = './milestone_models/res_net_aira2_poc07.h5'

print(f'using: {fmodel}')

def categorize_values(values, cats):
    _temp_values = np.zeros_like(values, dtype=int)
    _rules = _rules = [(True if left is None else values >= left) & (True if right is None else values < right) for left, right in cats]
    for idx, _rule in enumerate(_rules):
            _temp_values[_rule] = idx
    return _temp_values
    
                                          
_m_binary = [1., 5., 10., 15., 20., 25., 30., ]
model = models.model_perc_like((None, None, 147), output_size=len(_m_binary))
model.load_weights(fmodel)
print(model.summary())
'''
print('layer -3 weights info:',
      '\nmax:', model.layers[-4].get_weights()[0].max(),
      '\nmin:', model.layers[-4].get_weights()[0].min(),
      '\nsum:', model.layers[-4].get_weights()[0].sum(),
      '\nsquared sum:', (model.layers[-4].get_weights()[0]**2).sum(),
      '\nbias:', model.layers[-4].get_weights()[-1].mean(),
      )
      
print('last layer weights info:',
      '\nmax:', model.layers[-1].get_weights()[0].max(),
      '\nmin:', model.layers[-1].get_weights()[0].min(),
      '\nsum:', model.layers[-1].get_weights()[0].sum(),
      '\nsquared sum:', (model.layers[-1].get_weights()[0]**2).sum(),
      '\nbias:', model.layers[-1].get_weights()[-1],
      )
'''
_norms = {'hgtprs': 5800., 
          'tmpprs': 204., 
          'rhprs': 100., 
          'dptprs': 298., 
          'wind_speed': 30., 
          # 'wind_dir': 360., 
          'tmpsfc': 305., 
          'tmp2m': 310., 
          'rh2m': 100., 
          # 'wind_direction': 360., 
          'wind_magnitude': 30., 
          'acpcpsfc': 150., 
          'cpratsfc': 150.,
          'apcpsfc': 100.,
          'crainsfc': 100.,
          'pratesfc': 100., 
          'capesfc': 50.,
          }
          
_l_norms = {'precipitation': 100.,
            }
_l_bias_2 = {'tmax': 273.15,
              }
_l_norms_2 = {'tmax': 1.,
              }
'''
cat9 = [[None, 0.005],
        [0.005, 0.01],
        [0.01,  0.03],
        [0.03,  0.05],
        [0.05,  0.10],
        [0.10,  0.15],
        [0.15,  0.25],
        [0.25,  0.30],
        [0.30,  None]]
        
cat4 = [[None, 0.01],
        [0.01, 0.15],
        [0.15, 0.30],
        [0.30, None]]
'''
# orig
cat4_0 = [[None, 0.01],
          [0.01, 0.15],
          [0.15, 0.30],
          [0.30, None]]
# 1mm  
cat4_1 = [[None, 0.01],
          [0.01, 0.02],
          [0.02, 0.03],
          [0.03, None]]
# 5mm    
cat4_3 = [[None, 0.05],
          [0.05, 0.10],
          [0.10, 0.15],
          [0.15, None]]
# 10mm
cat4_2 = [[None, 0.10],
          [0.10, 0.20],
          [0.20, 0.30],
          [0.30, None]]
# 20mm     
cat4_4 = [[None, 0.20],
          [0.20, 0.40],
          [0.40, 0.60],
          [0.60, None]]
# 25mm     
cat4_5 = [[None, 0.25],
          [0.25, 0.50],
          [0.50, 0.75],
          [0.75, None]]
'''
_cat = [[None, 0.005],
        [0.005, 0.01],
        [0.01,  0.02],
        [0.02,  0.03],
        [0.03,  0.04],
        [0.04,  0.05],
        [0.05,  0.06],
        [0.06,  0.07],
        [0.07,  0.08],
        [0.08,  0.09],
        [0.09,  0.10],
        [0.10,  0.15],
        [0.15,  0.20],
        [0.20,  0.30],
        [0.30,  0.40],
        [0.40,  None]]

_cats = np.array([0., 1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 15., 20., 30., 40., 50.])
'''
_cat = [[None,  0.005],
        [0.005,  0.01],
        [0.01,   0.05],
        [0.05,   0.10],
        [0.10,   0.15],
        [0.15,   0.30],
        [0.30,   None]]

_cats = np.array([0., 1., 5., 10., 15., 30., 50.])                            
# _cat = [cat4_0, cat4_2, cat4_3, cat4_4, cat4_5]

# cat9_mids = np.asanyarray([0.0025, 0.0075, 0.02, 0.04, 0.075, 0.125, 0.2, 0.275, 0.65])
# cat4_mids = np.asanyarray([0.005, 0.08, 0.225, 0.65])
        
# dot9 = [0, 1, 3, 5, 10, 15, 25, 30, 100]
# dot4 = [0, 8, 23, 65]

# rules = {4: {'cat': cat4, 'dot': dot4, 'mids': cat4_mids},
#          9: {'cat': cat9, 'dot': dot9, 'mids': cat9_mids}}

# rule = 9
use_test = True
datasets = Dataset(input_path,
                   output_path,
                   dates,
                   # stacking=True,
                   # cat=_cat,
                   # norms=_norms,
                   # l_norms=[_l_norms, _l_norms_2],
                   # l_bias=[{}, _l_bias_2],
                   # test_slice_head=use_test,
                   scale=_scale,
                   ptp=_ptp,
                   m_binary=_m_binary,
                   )

# print(datasets._x_test.min(axis=(0, 1, 2))[:145], datasets._x_test.max(axis=(0, 1, 2))[:145], datasets._x_test.ptp(axis=(0, 1, 2))[:145])
# print(datasets._y_test.min(axis=(0, 1, 2)), datasets._y_test.max(axis=(0, 1, 2)), datasets._y_test.ptp(axis=(0, 1, 2)))
# input()

proj = ccrs.epsg(3857)

# total_power = []
def prepare_ax(datasets, *args, proj=proj):
    ax = plt.subplot(*args, projection=proj)
    ax.set_extent((datasets.general_lons.min(),
                   datasets.general_lons.max(),
                   datasets.general_lats.min(),
                   datasets.general_lats.max()),
                  crs=ccrs.PlateCarree())
    return ax
    
    
def prepare_coastline(ax):
    ax.coastlines(color='white', alpha=0.8)
    ax.gridlines(crs=ccrs.PlateCarree(), linestyle='--',
                 color='gray', draw_labels=True, alpha=0.8)


def multi_cat_to_c100(y, cats=_cat):
    # spread percentages uniformly across 0.01mm resolution
    # and average it
    y = np.asanyarray(y)
    weights = np.ones((*y.shape[1:3], 100))
    
    for y_c4, cat in zip(y, _cat):
        for y_ci, c in zip(y_c4.transpose((2, 0, 1)), cat):
            # y_ci is a grid
            left, right = c
            if right is None: right = 1.
            if left is None: left = 0.
            weights[..., slice(int(left * 100.), int(right * 100.))] *= y_ci[..., None]
    return weights / weights.sum(axis=-1, keepdims=True)
    

def multi_cat_to_numerical(y, cats=_cat, _type='max'):
    weights = multi_cat_to_c100(y, cats)
    if _type == 'max':
        return np.argmax(weights, axis=-1) # 1mm resolution
    elif _type == 'avg':
        return weights.dot(np.arange(100))
        

x_test, y_test = datasets.get_test(sample_size='all')
        
# test data
_in = x_test # datasets._x_test
_out = y_test # [:, 1:-1, 1:-1] # datasets._y_test_numerical
# _out_cat = datasets._y_test
    
# y_test = np.asanyarray(_out_cat).transpose((1, 0, 2, 3, 4))

    
print('full eval:', model.evaluate(_in, _out))
# print('partial eval:', model.evaluate(_in[::6], _out[::6]))
# print(_in.max())
# print(_out.max())
# c_test = grid_tricks.Chunks(_in.shape[1:3])
# _in_chunks = c_test.gen_chunks(_in)
# _out_chunks = c_test.gen_chunks(_out)

# print('chunks eval:', model.evaluate(_in_chunks, _out_chunks))

# yhat_chunks = model.predict(_in_chunks)
# yhat_chunks = _in_chunks * 0.

# yhat_full = model.predict(_in)
dists = [(0, -2), (1, -1), (2, None)]
around = [(slice(*k), slice(*l)) for k, l in itertools.product(dists, dists)]

# test plot          
yhat = np.asanyarray(model.predict(x_test))
y_obs = np.asanyarray(y_test)

def gen_precision_recall(threshold=0.5):
    yhat_neighbor = np.pad(yhat[..., 1], ((0, 0), (0, 0), (1, 1), (1, 1)), constant_values=np.nan)
    yhat_neighbor = np.nansum([yhat_neighbor[(..., *a)]**2 for a in around], axis=0)**0.5
    yhat_neighbor = yhat_neighbor > threshold
    yhat_model = yhat[..., 1] > threshold
    y_observed = y_obs[..., 1] > threshold
    
    tp = np.nansum( yhat_model *  y_observed)
    fp = np.nansum( yhat_model * ~y_observed)
    fn = np.nansum(~yhat_model *  y_observed)
    
    tp_n = np.nansum( yhat_neighbor *  y_observed)
    fp_n = np.nansum( yhat_neighbor * ~y_observed)
    fn_n = np.nansum(~yhat_neighbor *  y_observed)
    
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    
    precision_n = tp_n / (tp_n + fp_n)
    recall_n = tp_n / (tp_n + fn_n)
    
    return precision, recall, precision_n, recall_n, #  tp, fp, fn, tp_n, fp_n, fn_n


values = []
for t in np.arange(0.05, 1., 0.05):
    values.append(gen_precision_recall(t))
    
values = np.asanyarray(values).T

f1 = 2 * values[0] * values[1] / (values[0] + values[1])
f1_n = 2 * values[2] * values[3] / (values[2] + values[3])

print('f1 score/threshold', np.max(f1), np.arange(0.05, 1., 0.05)[np.argmax(f1)], values[:2, np.argmax(f1)])
print('f1 score/threshold', np.max(f1_n), np.arange(0.05, 1., 0.05)[np.argmax(f1_n)], values[2:, np.argmax(f1_n)])

best_thres = np.arange(0.05, 1., 0.05)[np.argmax(f1)]
best_thres_n = np.arange(0.05, 1., 0.05)[np.argmax(f1_n)]

plt.plot(np.arange(0.05, 1., 0.05), values[0], label='precision')
plt.plot(np.arange(0.05, 1., 0.05), values[1], label='recall')
plt.plot(np.arange(0.05, 1., 0.05), values[2], label='precision_n')
plt.plot(np.arange(0.05, 1., 0.05), values[3], label='recall_n')

plt.plot(np.arange(0.05, 1., 0.05), 2 * values[0] * values[1] / (values[0] + values[1]), label='f1')
plt.plot(np.arange(0.05, 1., 0.05), 2 * values[2] * values[3] / (values[2] + values[3]), label='f1_n')

# plt.plot(np.arange(0.05, 1., 0.05), (values[0]**2 + values[1]**2) * 0.5, label='test')
# plt.plot(np.arange(0.05, 1., 0.05), (values[2]**2 + values[3]**2) * 0.5, label='test_n')
plt.legend()

plt.savefig('./imgs/test_prec_recall.png')
plt.close()

'''
plt.plot(np.arange(0.05, 1., 0.05), values[4], label='tpr')
plt.plot(np.arange(0.05, 1., 0.05), values[5], label='fpr')
plt.plot(np.arange(0.05, 1., 0.05), values[6], label='fnr')
plt.plot(np.arange(0.05, 1., 0.05), values[7], label='tpr_n')
plt.plot(np.arange(0.05, 1., 0.05), values[8], label='fpr_n')
plt.plot(np.arange(0.05, 1., 0.05), values[9], label='fnr_n')
plt.legend()

plt.savefig('./imgs/test_tp_fp_fn.png')
plt.close()
'''

i = 0
# categorical
for j in range(24):
    rows = 3
    fig = plt.figure(figsize=(7. * len(_m_binary), 7. * rows))
    for idx, y in enumerate(yhat[:, j]):
        plt.subplot(rows, len(_m_binary), idx + 1)
        plt.pcolormesh(y[..., 1], cmap='cividis', vmin=0., vmax=1., shading='auto')
        plt.colorbar()
        
    for idx, y in enumerate(y_obs[:, j]):
        plt.subplot(rows, len(_m_binary), idx + 1 + len(_m_binary))
        plt.pcolormesh(y[..., 1], cmap='cividis', vmin=0., vmax=1., shading='auto')
        plt.colorbar()
    
    maps = []
    for idx, _info in enumerate(np.concatenate((yhat[:, j:j+1], y_obs[:, j:j+1]), axis=1)):
        # print('_info shape', _info.shape)
        plt.subplot(rows, len(_m_binary), idx + 1 + 2 * len(_m_binary))
        _plot = (_info[0] >= best_thres) - _info[1]
        _plot[(_info[0] < best_thres) & (_info[1] < best_thres)] = np.nan
        maps.append(_plot[..., 1])
        plt.pcolormesh(_plot[..., 1], cmap='brg', vmin=-1., vmax=1., shading='auto')
        plt.colorbar()
    
    maps = np.asanyarray(maps)
    total = np.sum(~np.isnan(maps))
    fn = np.sum(maps < -0.5)
    tp = np.sum((maps >= -0.5) & (maps < 0.5))
    fp = np.sum(maps >= 0.5)
    
    fig.suptitle('white: correct no rain prediction | red: correct rain prediction | green: false positive | blue: false negative\nv < -0.5: %f | v >= -0.5 & v < 0.5: %f | v >= 0.5: %f'%(fn / total, tp / total, fp / total))    
    plt.savefig(f'./imgs/test_plot_batch_%d_hour_%d_{dtNow:%Y%m%dT%H%M}.png'%(i, j))
    plt.close()
    
    
    fig = plt.figure(figsize=(7. * len(_m_binary), 7. * rows))
    for idx, y in enumerate(yhat[:, j]):
        plt.subplot(rows, len(_m_binary), idx + 1)
        _plot = np.pad(y[..., 1], ((1, 1), (1, 1)), constant_values=np.nan)
        _plot = np.nansum([_plot[a]**2 for a in around], axis=0)**0.5
        plt.pcolormesh(_plot, cmap='cividis', vmin=0., vmax=1., shading='auto')
        plt.colorbar()
        
    for idx, y in enumerate(y_obs[:, j]):
        plt.subplot(rows, len(_m_binary), idx + 1 + len(_m_binary))
        _plot = y[..., 1]
        plt.pcolormesh(_plot, cmap='cividis', vmin=0., vmax=1., shading='auto')
        plt.colorbar()
    
    maps = []
    for idx, _info in enumerate(np.concatenate((yhat[:, j:j+1], y_obs[:, j:j+1]), axis=1)):
        # print('_info shape', _info.shape)
        plt.subplot(rows, len(_m_binary), idx + 1 + 2 * len(_m_binary))
        _plot_pred = np.pad(_info[0, ..., 1], ((1, 1), (1, 1)), constant_values=np.nan)
        _plot_pred = np.nansum([_plot_pred[a]**2 for a in around], axis=0)**0.5
        
        _plot = (_plot_pred >= best_thres_n) - _info[1, ..., 1]
        _plot[(_plot_pred < best_thres_n) & (_info[1, ..., 1] < best_thres_n)] = np.nan
        maps.append(_plot)
        plt.pcolormesh(_plot, cmap='brg', vmin=-1., vmax=1., shading='auto')
        plt.colorbar()
    
    maps = np.asanyarray(maps)
    total = np.sum(~np.isnan(maps))
    fn = np.sum(maps < -0.5)
    tp = np.sum((maps >= -0.5) & (maps < 0.5))
    fp = np.sum(maps >= 0.5)
    
    fig.suptitle('white: correct no rain prediction | red: correct rain prediction | green: false positive | blue: false negative\nv < -0.5: %f | v >= -0.5 & v < 0.5: %f | v >= 0.5: %f'%(fn / total, tp / total, fp / total))  
    plt.savefig(f'./imgs/test_neighbor_plot_batch_%d_hour_%d_{dtNow:%Y%m%dT%H%M}.png'%(i, j))
    plt.close()
    
    
"""
# yhat = c_test.regen_maps(yhat_chunks)
# _out = c_test.regen_maps(_out_chunks)
# print(_out.max())
# print(yhat.shape, _out.shape)

datasets.general_lats = datasets.general_lats # [1:-1]
datasets.general_lons = datasets.general_lons # [1:-1]

# yhat_full = yhat_full[:, :_out.shape[1], :_out.shape[2]]

# if isinstance(yhat, list):
#     yhat = yhat[-1]
'''    
vmin, vmax = 0., None # _out.max() * _l_norms['precipitation']

days_test = 5
_size = datasets.idx_y_test.size // days_test # normally 24h?



_slice = slice(0, 40)
'''

# frc_maps = []
frc_full_maps = []
obs_maps = []

for idx, (l, y_f) in enumerate(zip(_out, yhat_full)):

    if not use_test:
        # date = datasets.dates[-days_test:][idx // _size]
        pass
    else:
        date = datasets.test_dates.repeat(24)[idx]
    # print(date, idx % 24)
    
    # y = y[_slice]
    # l = l[_slice]
    
    # res_cat = multi_cat_to_c100([y[y_idx] for y in yhat])
    # yhat_plot = y[..., 0] * datasets.y_ptp + datasets.y_min
    
    yhat_full_plot = datasets.rescale_y(y_f[..., 0])
    y_test_plot = datasets.rescale_y(l[..., 0])
    
    '''
    yhat_full_plot = y_f[..., 0] * datasets.y_ptp # + datasets.y_min
    y_test_plot = l[..., 0] * datasets.y_ptp # + datasets.y_min
    
    yhat_full_plot[yhat_full_plot > 1.] = (yhat_full_plot[yhat_full_plot > 1.] - 1.) * 3. + 1. 
    y_test_plot[y_test_plot > 1.] = (y_test_plot[y_test_plot > 1.] - 1.) * 3. + 1.
    '''
    
    # res_temp = y[..., 1]
    # yhat_plot = yhat_plot.clip(0., 10.)
    # y_test_plot = y_test_plot.clip(0., 10.)
    
    max_plot = max(min(30., max(yhat_full_plot.max(), y_test_plot.max())), 1.)
    ###
    # frc_maps.append(y.dot(rules[rule]['dot']))
    # frc_maps.append(yhat_plot)
    frc_full_maps.append(yhat_full_plot)
    obs_maps.append(y_test_plot)
    ###
    continue
    '''
    ### PLOT CATEGORIES
    num_categories = y.shape[-1]
    fig = plt.figure(figsize=(7. * num_categories, 7.))
    
    for cat in range(num_categories):
    
        plt.subplot(1, num_categories, cat + 1)
        plt.pcolormesh(datasets.general_lons, datasets.general_lats, y[..., cat], vmin=0., vmax=1., cmap='cividis')
        plt.xlabel('lon')
        plt.ylabel('lat')
        plt.title('frc cat %d'%cat)
        plt.colorbar()
    fig.suptitle('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), time_idx) + '\n' + str(rules[rule]['dot']))
    
    plt.savefig(f"./imgs/cat_%s_%s_%d_{suffix}.png"%(fmodel[:-3].split('/')[-1], date.strftime("%Y%m%d"), idx))
    plt.close()
    ###
    '''
    
    ### PLOT FRC + DIFF + OBS
    fig = plt.figure(figsize=(21., 7.))
    
    # plt.subplot(1, 3, 1)
    ax = prepare_ax(datasets, 1, 3, 1)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   yhat_full_plot,
                   vmin=0.,
                   vmax=max_plot,
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('frc full')
    plt.colorbar()
    
    diff = yhat_full_plot - y_test_plot
    
    ax = prepare_ax(datasets, 1, 3, 2)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   diff,
                   vmin=-abs(diff).max(),
                   vmax=abs(diff).max(),
                   cmap='bwr',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('diff')
    plt.colorbar()
    
    # plt.subplot(1, 3, 3)
    ax = prepare_ax(datasets, 1, 3, 3)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   y_test_plot,
                   vmin=0.,
                   vmax=max_plot,
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('obs')
    plt.colorbar()
    
    mse = np.mean((yhat_full_plot - y_test_plot)**2)
    mape = np.mean(abs((yhat_full_plot - y_test_plot) / y_test_plot))
    fig.suptitle('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), idx % 24) + ' | mse: %.3f'%mse + ' | rmse: %.3f'%np.sqrt(mse) + ' | mape: %.3f'%mape)
    
    plt.savefig(f'./imgs/frc_diff_obs_%s_%s_%d_{suffix}.png'%(fmodel[:-3].split('/')[-1], date.strftime("%Y%m%d"), idx))
    plt.close()
    ###
    
    
    
    '''
    ### PLOT FRC + WRF + OBS
    fig = plt.figure(figsize=(21., 7.))
    
    # plt.subplot(1, 3, 1)
    ax = prepare_ax(datasets, 1, 3, 1)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   y.dot(rules[rule]['dot']),
                   # y[..., 0]  * _l_norms['precipitation'],
                   vmin=0.,
                   vmax=min(vmax, _l_norms['precipitation']),
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('frc dot')
    plt.colorbar()
    
    # plt.subplot(1, 3, 2)
    ax = prepare_ax(datasets, 1, 3, 2)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   (datasets._x[time_idx, ..., datasets._var_header[ 'apcpsfc']] - datasets._x[time_idx - 1, ..., datasets._var_header[ 'apcpsfc']]) * datasets._per_variable_norm[ 'apcpsfc'] +\
                   (datasets._x[time_idx, ..., datasets._var_header['acpcpsfc']] - datasets._x[time_idx - 1, ..., datasets._var_header['acpcpsfc']]) * datasets._per_variable_norm['acpcpsfc'],
                   vmin=0.,
                   vmax=min(vmax, _l_norms['precipitation']),
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('wrf apcpsfc + acpcpsfc')
    plt.colorbar()
    
    # plt.subplot(1, 3, 3)
    ax = prepare_ax(datasets, 1, 3, 3)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   l[..., 0] * _l_norms['precipitation'],
                   vmin=0.,
                   vmax=min(vmax, _l_norms['precipitation']),
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('obs')
    plt.colorbar()
    
    fig.suptitle('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), time_idx))
    
    plt.savefig(f'./imgs/frc_wrf_obs_%s_%s_%d_{suffix}.png'%(fmodel[:-3].split('/')[-1], date.strftime("%Y%m%d"), idx))
    plt.close()
    ###
    '''
    '''
    ### PLOT FRC + OBS
    fig = plt.figure(figsize=(14., 7.))
    
    
    '''
    ax = plt.subplot(1, 2, 1, projection=proj)
    ax.set_extent((datasets.general_lons.min(),
                   datasets.general_lons.max(),
                   datasets.general_lats.min(),
                   datasets.general_lats.max()),
                  crs=ccrs.PlateCarree())
    '''
    ax = prepare_ax(datasets, 1, 2, 1)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   yhat_plot,
                   vmin=0.,
                   vmax=max_plot,
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    '''
    ax.coastlines(color='white', alpha=0.8)
    ax.gridlines(crs=ccrs.PlateCarree(), linestyle='--',
                 color='gray', draw_labels=True, alpha=0.8)
    '''
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('frc chunks')
    plt.colorbar()
    
    # plt.subplot(1, 2, 2)
    ax = prepare_ax(datasets, 1, 2, 2)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   y_test_plot,
                   vmin=0.,
                   vmax=max_plot,
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('obs')
    plt.colorbar()
    
    mse = np.mean((yhat_plot - y_test_plot)**2)
    fig.suptitle('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), idx % 24) + ' | mse: %.3f'%mse)
    # fig.suptitle('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), time_idx) + ' | mse: %.3f'%np.mean((y[..., 0] * _l_norms['precipitation'] - l[..., 0] * _l_norms['precipitation'])**2))
    plt.tight_layout()
    
    plt.savefig(f'./imgs/frc_obs_%s_%s_%d_{suffix}.png'%(fmodel[:-3].split('/')[-1], date.strftime("%Y%m%d"), idx))
    plt.close()
    ###
    '''

    ### PLOT FRC FULL + OBS
    fig = plt.figure(figsize=(14., 7.))
    
    
    '''
    ax = plt.subplot(1, 2, 1, projection=proj)
    ax.set_extent((datasets.general_lons.min(),
                   datasets.general_lons.max(),
                   datasets.general_lats.min(),
                   datasets.general_lats.max()),
                  crs=ccrs.PlateCarree())
    '''
    ax = prepare_ax(datasets, 1, 2, 1)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   yhat_full_plot,
                   vmin=0.,
                   vmax=max_plot,
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    '''
    ax.coastlines(color='white', alpha=0.8)
    ax.gridlines(crs=ccrs.PlateCarree(), linestyle='--',
                 color='gray', draw_labels=True, alpha=0.8)
    '''
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('frc full')
    plt.colorbar()
    
    # plt.subplot(1, 2, 2)
    ax = prepare_ax(datasets, 1, 2, 2)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   y_test_plot,
                   vmin=0.,
                   vmax=max_plot,
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('obs')
    plt.colorbar()
    
    mse = np.mean((yhat_full_plot - y_test_plot)**2)
    mape = np.mean(abs((yhat_full_plot - y_test_plot) / y_test_plot))
    fig.suptitle('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), idx % 24) + ' | mse: %.3f'%mse + ' | rmse: %.3f'%np.sqrt(mse) + ' | mape: %.3f'%mape)
    # fig.suptitle('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), time_idx) + ' | mse: %.3f'%np.mean((y[..., 0] * _l_norms['precipitation'] - l[..., 0] * _l_norms['precipitation'])**2))
    plt.tight_layout()
    
    plt.savefig(f'./imgs/frc_full_obs_%s_%s_%d_{suffix}.png'%(fmodel[:-3].split('/')[-1], date.strftime("%Y%m%d"), idx))
    plt.close()
    ###
    
    '''
    ### PLOT FRC + OBS
    fig = plt.figure(figsize=(14., 7.))
    
    
    
    ax = plt.subplot(1, 2, 1, projection=proj)
    ax.set_extent((datasets.general_lons.min(),
                   datasets.general_lons.max(),
                   datasets.general_lats.min(),
                   datasets.general_lats.max()),
                  crs=ccrs.PlateCarree())
    
    ax = prepare_ax(datasets, 1, 2, 1)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats[_slice],
                   res_temp,
                   # vmin=0.,
                   # vmax=vmax,
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    
    ax.coastlines(color='white', alpha=0.8)
    ax.gridlines(crs=ccrs.PlateCarree(), linestyle='--',
                 color='gray', draw_labels=True, alpha=0.8)
    
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('frc temp')
    plt.colorbar()
    
    # plt.subplot(1, 2, 2)
    ax = prepare_ax(datasets, 1, 2, 2)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats[_slice],
                   l[..., 1],
                   # vmin=0.,
                   # vmax=vmax,
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('obs')
    plt.colorbar()
    
    mse = np.mean(res_temp - l[..., 1])**2
    fig.suptitle('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), time_idx) + ' | mse: %.3f'%mse)
    plt.tight_layout()
    
    plt.savefig(f'./imgs/temp_frc_obs_%s_%s_%d_{suffix}.png'%(fmodel[:-3].split('/')[-1], date.strftime("%Y%m%d"), idx))
    plt.close()
    ###
    ''' 
    '''
    ### PLOT CONTINGENCY
    
    contingency = np.zeros((num_categories, num_categories))
    flat_label = l_cat.reshape(-1, num_categories)
    flat_yhat = y.reshape(-1, num_categories)
    where_argmax = np.argmax(flat_yhat, axis=1)
    # one_hot_yhat = np.zeros_like(flat_yhat)
    # one_hot_yhat[(tuple(np.arange(where_argmax.size)), tuple(where_argmax))] = 1.
    
    w = np.eye(rule)
    
    # print(flat_yhat.shape, one_hot_yhat.shape)
    
    occurr = []
    for cat in range(num_categories):
        cat_true = np.argmax(flat_label, axis=1) == cat
        reindexing = np.argmax(flat_yhat[cat_true], axis=1)
        # reindexing_values = flat_yhat[cat_true].dot(rules[rule]['dot'])
        # reindexing = categorize_values(reindexing_values, rules[rule]['cat'])
        contingency[cat, :] = w[reindexing].sum(axis=0)
        contingency[cat, :] /= contingency[cat, :].sum() or 1.
        occurr.append(cat_true.sum())
        # contingency[cat, :] = one_hot_yhat[cat_true].sum(axis=0) / one_hot_yhat[cat_true].sum()
        
    fig, ax = plt.subplots(figsize=(7., 7.))
        
    ax.imshow(contingency, vmin=0., vmax=max(1., contingency.max()), cmap='cividis')
    ax.set_xticks(np.arange(num_categories))
    ax.set_yticks(np.arange(num_categories))
    ax.set_yticklabels(occurr)
    ax.set_xlabel('frc')
    ax.set_ylabel('ocorrencias')
    
    # ax2 = ax.twinx()
    # ax2.imshow()
    # ax2.set_yticks(np.arange(num_categories))
    # ax2.set_yticklabels(occurr)
    # ax2.set_ylabel('total occurrence')
    
    thres = contingency.ptp() / 2.
    
    for i in range(num_categories):
        for j in range(num_categories):
            ax.text(j, i, '%.3f'%contingency[i, j], ha='center', va='center', color='k' if contingency[i, j] > thres else 'w')
    
    ax.set_title('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), time_idx))
    fig.tight_layout()
    
    plt.savefig(f'./imgs/contingency_%s_%s_%d_{suffix}.png'%(fmodel[:-3].split('/')[-1], date.strftime("%Y%m%d"), idx))
    plt.close()
    ###
    '''
    
    '''
    ### PLOT CONTINGENCY MEAN
    
    contingency = np.zeros((num_categories, num_categories))
    flat_label = l_cat.reshape(-1, num_categories)
    flat_yhat = y.reshape(-1, num_categories)
    where_argmax = flat_yhat.dot(np.arange(num_categories)).astype(int)
    # one_hot_yhat = np.zeros_like(flat_yhat)
    # one_hot_yhat[(tuple(np.arange(where_argmax.size)), tuple(where_argmax))] = 1.
    
    w = np.eye(rule)
    
    # print(flat_yhat.shape, one_hot_yhat.shape)
    
    occurr = []
    for cat in range(num_categories):
        cat_true = np.argmax(flat_label, axis=1) == cat
        reindexing = flat_yhat[cat_true].dot(np.arange(num_categories) + 0.5).astype(int)
        # reindexing_values = flat_yhat[cat_true].dot(rules[rule]['dot'])
        # reindexing = categorize_values(reindexing_values, rules[rule]['cat'])
        contingency[cat, :] = w[reindexing].sum(axis=0)
        contingency[cat, :] /= contingency[cat, :].sum() or 1.
        occurr.append(cat_true.sum())
        # contingency[cat, :] = one_hot_yhat[cat_true].sum(axis=0) / one_hot_yhat[cat_true].sum()
        
    fig, ax = plt.subplots(figsize=(7., 7.))
        
    ax.imshow(contingency, vmin=0., vmax=max(1., contingency.max()), cmap='cividis')
    ax.set_xticks(np.arange(num_categories))
    ax.set_yticks(np.arange(num_categories))
    ax.set_yticklabels(occurr)
    ax.set_xlabel('frc')
    ax.set_ylabel('ocorrencias')
    
    # ax2 = ax.twinx()
    # ax2.imshow()
    # ax2.set_yticks(np.arange(num_categories))
    # ax2.set_yticklabels(occurr)
    # ax2.set_ylabel('total occurrence')
    
    thres = contingency.ptp() / 2.
    
    for i in range(num_categories):
        for j in range(num_categories):
            ax.text(j, i, '%.3f'%contingency[i, j], ha='center', va='center', color='k' if contingency[i, j] > thres else 'w')
    
    ax.set_title('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), time_idx))
    fig.tight_layout()
    
    plt.savefig(f'./imgs/contingency_mean_%s_%s_%d_{suffix}.png'%(fmodel[:-3].split('/')[-1], date.strftime("%Y%m%d"), idx))
    plt.close()
    ###
    '''
    
# frc_maps = np.asanyarray(frc_maps)
frc_full_maps = np.asanyarray(frc_full_maps)
obs_maps = np.asanyarray(obs_maps)

#
frc_maps = frc_full_maps
#

mae_map = abs(frc_maps - obs_maps).mean(axis=0)
mse_map = np.power(frc_maps - obs_maps, 2).mean(axis=0)
mse_99_map = mse_map.copy()
mse_99_map[mse_99_map >= np.percentile(mse_99_map, 99)] = np.nan
p0_map = ((frc_maps  < 1.) & (obs_maps  < 1.)).sum(axis=0) / (obs_maps  < 1.).sum(axis=0)
p1_map = ((frc_maps >= 1.) & (obs_maps >= 1.)).sum(axis=0) / (obs_maps >= 1.).sum(axis=0)
p5_map = ((frc_maps >= 5.) & (obs_maps >= 5.)).sum(axis=0) / (obs_maps >= 5.).sum(axis=0)

obs_mean = np.mean(obs_maps, axis=0)
g_obs_mean = np.mean(obs_maps)
local_willmott_map = 1. - np.sum(np.power(frc_maps - obs_maps, 2), axis=0) /\
                          np.sum(np.power(abs(frc_maps - obs_mean) +\
                                          abs(obs_maps - obs_mean), 2), axis=0)
global_willmott_map = 1. - np.sum(np.power(frc_maps - obs_maps, 2), axis=0) /\
                           np.sum(np.power(abs(frc_maps - g_obs_mean) +\
                                           abs(obs_maps - g_obs_mean), 2), axis=0)
tp = ((frc_maps >= 1.) & (obs_maps >= 1.)).sum(axis=0)
fp = ((frc_maps >= 1.) & (obs_maps < 1.)).sum(axis=0)
fn = ((frc_maps < 1.) & (obs_maps >= 1.)).sum(axis=0)
precision_1mm = tp / np.maximum((tp + fp), 1.)
recall_1mm = tp / np.maximum((tp + fn), 1.)
### PLOT FINAL STATS
cols = 9
fig = plt.figure(figsize=(7. * cols, 7.))

ax = prepare_ax(datasets, 1, cols, 1)
plt.pcolormesh(datasets.general_lons,
               datasets.general_lats,
               mae_map,
               vmin=0.,
               # vmax=vmax,
               cmap='cividis',
               transform=ccrs.PlateCarree())
prepare_coastline(ax)
plt.xlabel('lon')
plt.ylabel('lat')
plt.title('mae')
plt.colorbar()

ax = prepare_ax(datasets, 1, cols, 2)
plt.pcolormesh(datasets.general_lons,
               datasets.general_lats,
               mse_map,
               vmin=0.,
               # vmax=vmax,
               cmap='cividis',
               transform=ccrs.PlateCarree())
prepare_coastline(ax)
plt.xlabel('lon')
plt.ylabel('lat')
plt.title('mse')
plt.colorbar()

ax = prepare_ax(datasets, 1, cols, 3)
plt.pcolormesh(datasets.general_lons,
               datasets.general_lats,
               mse_99_map,
               vmin=0.,
               # vmax=vmax,
               cmap='cividis',
               transform=ccrs.PlateCarree())
prepare_coastline(ax)
plt.xlabel('lon')
plt.ylabel('lat')
plt.title('mse | percentil 99')
plt.colorbar()

ax = prepare_ax(datasets, 1, cols, 4)
plt.pcolormesh(datasets.general_lons,
               datasets.general_lats,
               p0_map,
               vmin=0.,
               vmax=1.,
               cmap='bwr',
               transform=ccrs.PlateCarree())
prepare_coastline(ax)
plt.xlabel('lon')
plt.ylabel('lat')
plt.title('acc prec < 1')
plt.colorbar()

ax = prepare_ax(datasets, 1, cols, 5)
plt.pcolormesh(datasets.general_lons,
               datasets.general_lats,
               p1_map,
               vmin=0.,
               vmax=1.,
               cmap='bwr',
               transform=ccrs.PlateCarree())
prepare_coastline(ax)
plt.xlabel('lon')
plt.ylabel('lat')
plt.title('acc prec >= 1')
plt.colorbar()

ax = prepare_ax(datasets, 1, cols, 6)
plt.pcolormesh(datasets.general_lons,
               datasets.general_lats,
               local_willmott_map,
               # vmin=0.,
               # vmax=1.,
               cmap='cividis',
               transform=ccrs.PlateCarree())
prepare_coastline(ax)
plt.xlabel('lon')
plt.ylabel('lat')
plt.title('concordancia de willmott - local mean')
plt.colorbar()
    
ax = prepare_ax(datasets, 1, cols, 7)
plt.pcolormesh(datasets.general_lons,
               datasets.general_lats,
               global_willmott_map,
               # vmin=-5.,
               # vmax=1.,
               cmap='cividis',
               transform=ccrs.PlateCarree())
prepare_coastline(ax)
plt.xlabel('lon')
plt.ylabel('lat')
plt.title('concordancia de willmott - global mean')
plt.colorbar()

ax = prepare_ax(datasets, 1, cols, 8)
plt.pcolormesh(datasets.general_lons,
               datasets.general_lats,
               precision_1mm,
               vmin=0.,
               vmax=1.,
               cmap='bwr',
               transform=ccrs.PlateCarree())
prepare_coastline(ax)
plt.xlabel('lon')
plt.ylabel('lat')
plt.title('precision at 1mm')
plt.colorbar()

ax = prepare_ax(datasets, 1, cols, 9)
plt.pcolormesh(datasets.general_lons,
               datasets.general_lats,
               recall_1mm,
               vmin=0.,
               vmax=1.,
               cmap='bwr',
               transform=ccrs.PlateCarree())
prepare_coastline(ax)
plt.xlabel('lon')
plt.ylabel('lat')
plt.title('recall at 1mm')
plt.colorbar()
    
fig.suptitle('stats')

plt.savefig(f"./imgs/stats_%s_{suffix}.png"%(fmodel[:-3].split('/')[-1]))
plt.close()

###
target_lat = np.argmin(abs(datasets.general_lats + 23.))
target_lon = np.argmin(abs(datasets.general_lons + 46.))

lat_slice = slice(target_lat - 0, target_lat + 1)
lon_slice = slice(target_lon - 0, target_lon + 1)

frc_graph = frc_maps[:, lat_slice, lon_slice].mean(axis=(1, 2))
obs_graph = obs_maps[:, lat_slice, lon_slice].mean(axis=(1, 2))

# dates = pd.date_range('2021-01-01', '2021-01-10T23', freq='H').to_pydatetime()

fig = plt.figure(figsize=(19.2, 10.8))


plt.bar(np.arange(obs_graph.size) + 0.1, obs_graph, color='b', label='obs merge', alpha=0.8, width=0.5)
plt.bar(np.arange(frc_graph.size) + 0.4, frc_graph, color='r', label='aira2 frc', alpha=0.8, width=0.5)
plt.grid('both')
plt.legend()
plt.tight_layout()

fig.suptitle('10 days hourly forecast at 23S46W')

plt.savefig('./imgs/just_a_regular_plot.png')
plt.close()

###

"""