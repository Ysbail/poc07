import tensorflow as tf
# gpus = tf.config.experimental.list_physical_devices('GPU')
# for gpu in gpus:
#   tf.config.experimental.set_memory_growth(gpu, True)
  
  
import requests
import src.models as models
import src.model_tricks as model_tricks
from keras.models import load_model
from src.dataset import Dataset
import numpy as np
from src.customs import (
    custom_acc,
    dunno_loss,
    custom_loss,
    custom_mape,
    above_1_acc,
    custom_acc_0,
    less_mistakes,
    false_positive,
    false_negative,
    custom_acc_loss,
    accuracy_entropy,
    cat_false_positive,
    cat_false_negative,
    prop_false_negative,
    cat_accuracy_entropy,
    cat_accuracy_entropy_score,
)
from keras.callbacks import (
    ModelCheckpoint,
    EarlyStopping,
    ReduceLROnPlateau
)
from keras.metrics import (
    SparseCategoricalAccuracy,
    sparse_categorical_accuracy,
)

# sparse_categorical_accuracy = SparseCategoricalAccuracy()

import pandas as pd
import datetime as dt

import tensorflow.keras.backend as K

import glob

import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.use("Agg")

K.clear_session()

dtNow = dt.datetime.utcnow()

input_path = '/storagefinep/wrf/dataset/{date:%Y%m%d}_wrf_dataset.nc'
output_path = '/storagefinep/merge/dataset/{date:%Y%m%d}_merge_dataset.nc'

tmax_url = 'http://ftp.cptec.inpe.br/modelos/tempo/SAMeT/DAILY/TMAX/%Y/%m/SAMeT_CPTEC_TMAX_%Y%m%d.nc'
output_path_2 = {'args': ('inmemory.nc', ),
                 'kwargs': lambda _date: {'memory': requests.get(_date.strftime(tmax_url)).content}, 
                }

epochs = 5000
batch_size = 32
best_model_pattern = "./models/best_{}.h5"
monitor_metric = 'val_loss' # 'val_categorical_cat_accuracy_entropy_score' # 'val_cat_accuracy_entropy'
verb = 2
earlystop_mode = 'min'
earlystop_monitor = 'val_loss' # 'val_categorical_cat_accuracy_entropy_score' # 'val_cat_accuracy_entropy'
earlystop_patience = 50

first = True
model = None

date_ini = '2021-01-01'
date_fin = '2021-07-25'

dates = pd.date_range(date_ini, date_fin, freq='D').to_pydatetime()

# norms
_norms = {'hgtprs': 5800., 
          'tmpprs': 204., 
          'rhprs': 100., 
          'dptprs': 298., 
          'wind_speed': 30., 
          # 'wind_dir': 360., 
          'tmpsfc': 305., 
          'tmp2m': 310., 
          'rh2m': 100., 
          # 'wind_direction': 360., 
          'wind_magnitude': 30., 
          'acpcpsfc': 150., 
          'cpratsfc': 150.,
          'apcpsfc': 100.,
          'crainsfc': 100.,
          'pratesfc': 100., 
          'capesfc': 50.,
          }

_scale = 1.
_ptp = 100.
_m_binary = [1., 5., 10., 15., 20., 25., 30., ]
# prepare datasets iter
datasets = Dataset(input_path,
                   output_path,
                   dates,
                   # stacking=True,
                   # cat=_cat,
                   # norms=_norms,
                   # l_norms=[_l_norms, _l_norms_2],
                   # l_bias=[{}, _l_bias_2]
                   scale=_scale,
                   ptp=_ptp,
                   m_binary=_m_binary,
                   )

print(datasets._input_var_order)
print(datasets.num_features)

print(datasets._output_var_order)
print(datasets.num_labels)

# for zone_idx, zone in enumerate(zones):
fmodel = f'./models/{dtNow:%Y%m%dT%H%M}_aira2_poc07.h5'
best_model = f'./models/bkp_{dtNow:%Y%m%dT%H%M}_aira2_poc07.h5' # best_model_pattern.format(date.strftime('%Y%m%d'))
checkpoint = ModelCheckpoint(best_model, monitor=monitor_metric, verbose=verb, save_best_only=True, mode=earlystop_mode)
earlystop = EarlyStopping(monitor=earlystop_monitor, patience=earlystop_patience, mode=earlystop_mode, verbose=verb, restore_best_weights=True)
callbacks_list = [checkpoint, earlystop]

first = True

for i, (date, dataset) in enumerate(datasets.iterdates()):
    
    x_train = dataset.x_train # dataset._x_train_chunks
    y_train = dataset.y_train # [:, 1:-1, 1:-1]
    
    x_valid = dataset.x_valid
    y_valid = dataset.y_valid # [:, 1:-1, 1:-1]
    
    if first:
        first = False
        K.clear_session()
        model = models.model_perc_like((None, None, dataset.num_features), output_size=len(_m_binary))
    else:
        model.load_weights(best_model)
    
    if glob.glob(fmodel) == []:
        print("[I] Saving first model")
        model.save(fmodel)
        
    if glob.glob(best_model) == []:
        model.save(best_model)
    
    checkpoint = ModelCheckpoint(best_model, monitor=monitor_metric, verbose=verb, save_best_only=True, mode=earlystop_mode)
    # earlystop = EarlyStopping(monitor=earlystop_monitor, patience=earlystop_patience, mode=earlystop_mode, verbose=verb, restore_best_weights=True)
    callbacks_list = [checkpoint, earlystop]
    
    model.fit(x_train,
              y_train,
              epochs=epochs,
              verbose=verb,
              validation_data=(x_valid,
                               y_valid),
              callbacks=callbacks_list)
              
    # train plot          
    yhat = np.asanyarray(model.predict(x_train[:24]))
    y_obs = np.asanyarray(y_train)[:, :24]
    
    # categorical
    for j in range(24):
        plt.figure(figsize=(7. * len(_m_binary), 7. * 2))
        for idx, y in enumerate(yhat[:, j]):
            plt.subplot(2, len(_m_binary), idx + 1)
            plt.pcolormesh(y[..., 1], cmap='cividis', vmin=0., vmax=1., shading='auto')
            plt.colorbar()
            
        for idx, y in enumerate(y_obs[:, j]):
            plt.subplot(2, len(_m_binary), idx + 1 + len(_m_binary))
            plt.pcolormesh(y[..., 1], cmap='cividis', vmin=0., vmax=1., shading='auto')
            plt.colorbar()
            
        plt.savefig(f'./imgs/train_plot_batch_%d_hour_%d_{dtNow:%Y%m%dT%H%M}.png'%(i, j))
        plt.close()
    
    
    # valid plot          
    yhat = np.asanyarray(model.predict(x_valid[:24]))
    y_obs = np.asanyarray(y_valid)[:, :24]
    
    # categorical
    for j in range(24):
        plt.figure(figsize=(7. * len(_m_binary), 7. * 2))
        for idx, y in enumerate(yhat[:, j]):
            plt.subplot(2, len(_m_binary), idx + 1)
            plt.pcolormesh(y[..., 1], cmap='cividis', vmin=0., vmax=1., shading='auto')
            plt.colorbar()
            
        for idx, y in enumerate(y_obs[:, j]):
            plt.subplot(2, len(_m_binary), idx + 1 + len(_m_binary))
            plt.pcolormesh(y[..., 1], cmap='cividis', vmin=0., vmax=1., shading='auto')
            plt.colorbar()
            
        plt.savefig(f'./imgs/valid_plot_batch_%d_hour_%d_{dtNow:%Y%m%dT%H%M}.png'%(i, j))
        plt.close()
        
    '''
    # numerical
    plt.figure(figsize=(7. * 24, 7. * 2))
    y_train_plot = dataset.rescale_y(y_train[:24, ..., 0])
    yhat_plot = dataset.rescale_y(yhat[..., 0])
    
    print(y_train_plot.shape)
    print(yhat_plot.shape)

    _max = np.max([np.nanmax(y_train_plot, axis=(1, 2)),
                   np.nanmax(yhat_plot, axis=(1, 2))],
                  axis=0)
                  
    print(_max.shape)
    
    for idx, y in enumerate(y_train_plot):
        plt.subplot(2, 24, idx + 1)
        plt.pcolormesh(y, cmap='cividis', vmin=0., vmax=max(1., _max[idx]))
        plt.colorbar()
        
    for idx, y in enumerate(yhat_plot):
        plt.subplot(2, 24, idx + 1 + 24)
        plt.pcolormesh(y, cmap='cividis', vmin=0., vmax=max(1., _max[idx]))
        plt.colorbar()
        
    plt.tight_layout()
    plt.savefig(f'./imgs/train_plot_batch_%d_{dtNow:%Y%m%dT%H%M}.png'%i)
        
    plt.close()

    # validation plot
    yhat = model.predict(x_valid[:24])
    plt.figure(figsize=(7. * 24, 7. * 2))
    
    y_valid_plot = dataset.rescale_y(y_valid[:24, ..., 0])
    yhat_plot = dataset.rescale_y(yhat[..., 0])
    
    print(y_valid_plot.shape)
    print(yhat_plot.shape)

    _max = np.max([np.nanmax(y_valid_plot, axis=(1, 2)),
                   np.nanmax(yhat_plot, axis=(1, 2))],
                  axis=0)
                  
    print(_max.shape)
    
    for idx, y in enumerate(y_valid_plot):
        plt.subplot(2, 24, idx + 1)
        plt.pcolormesh(y, cmap='cividis', vmin=0., vmax=max(1., _max[idx]))
        plt.colorbar()
        
    for idx, y in enumerate(yhat_plot):
        plt.subplot(2, 24, idx + 1 + 24)
        plt.pcolormesh(y, cmap='cividis', vmin=0., vmax=max(1., _max[idx]))
        plt.colorbar()
        
    plt.tight_layout()
    plt.savefig(f'./imgs/validation_plot_batch_%d_{dtNow:%Y%m%dT%H%M}.png'%i)
        
    plt.close()
    '''
    print("[I] Saving model")
    model.save(fmodel)
    
    
    
    
'''          
_l_norms = {'precipitation': 100.,
            }
            
_l_bias_2 = {'tmax': 273.15,
              }
_l_norms_2 = {'tmax': 400.,
              }
'''
'''
# cats
_cat = [[None, 0.005],
        [0.005, 0.01],
        [0.01,  0.02],
        [0.02,  0.03],
        [0.03,  0.04],
        [0.04,  0.05],
        [0.05,  0.06],
        [0.06,  0.07],
        [0.07,  0.08],
        [0.08,  0.09],
        [0.09,  0.10],
        [0.10,  0.15],
        [0.15,  0.20],
        [0.20,  0.30],
        [0.30,  0.40],
        [0.40,  None]]

_cats = np.array([0., 1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 15., 20., 30., 40., 50.])
'''
'''
_cat = [[None,  0.005],
        [0.005,  0.01],
        [0.01,   0.05],
        [0.05,   0.10],
        [0.10,   0.15],
        [0.15,   0.30],
        [0.30,   None]]

_cats = np.array([0., 1., 5., 10., 15., 30., 50.])
# orig
cat4_0 = [[None, 0.01],
          [0.01, 0.15],
          [0.15, 0.30],
          [0.30, None]]
# 1mm  
cat4_1 = [[None, 0.01],
          [0.01, 0.02],
          [0.02, 0.03],
          [0.03, None]]
# 5mm    
cat4_3 = [[None, 0.05],
          [0.05, 0.10],
          [0.10, 0.15],
          [0.15, None]]
# 10mm
cat4_2 = [[None, 0.10],
          [0.10, 0.20],
          [0.20, 0.30],
          [0.30, None]]
# 20mm     
cat4_4 = [[None, 0.20],
          [0.20, 0.40],
          [0.40, 0.60],
          [0.60, None]]
# 25mm     
cat4_5 = [[None, 0.25],
          [0.25, 0.50],
          [0.50, 0.75],
          [0.75, None]]
                    
# _cat = [cat4_0, cat4_1, cat4_2, cat4_3, cat4_4, cat4_5]
'''